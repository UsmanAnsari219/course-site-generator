/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.file;

import static coursesitegenerator.file.csgFiles.JSON_OFFICE_HOURS;
import static coursesitegenerator.file.csgFiles.JSON_RECITATIONS;
import static coursesitegenerator.file.csgFiles.JSON_SCHEDULES;
import static coursesitegenerator.file.csgFiles.JSON_STUDENTS;
import static coursesitegenerator.file.csgFiles.JSON_TEAMS;
import static coursesitegenerator.file.csgFiles.JSON_UNDERGRAD_TAS;
import djf.components.AppDataComponent;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Usman
 */
public class csgFilesTest {
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_RECITATIONS = "recitations";
    static final String JSON_SCHEDULES = "schedules";
    static final String JSON_STUDENTS = "students";
    static final String JSON_TEAMS = "teams";
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAYTIME = "dayTime";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta1";
    static final String JSON_TA2 = "ta2";
    static final String JSON_TYPE = "type";
    static final String JSON_DATE = "date";
    static final String JSON_TITLE = "title";
    static final String JSON_TOPIC = "topic";
    static final String JSON_TIMES = "time";
    static final String JSON_LINK = "link";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_NAMEP = "name";
    static final String JSON_COLOR = "color";
    static final String JSON_TEXTCOLOR = "textColor";
    static final String JSON_LINKP = "link";
    static final String JSON_FNAME = "fName";
    static final String JSON_LNAME = "lName";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_EMAIL = "email";
    
    public csgFilesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    

    /**
     * Test of loadData method, of class csgFiles.
     */
    @Test
    public void testLoadData() throws IOException {
        csgFiles fileMang = new csgFiles();
        String jsonFilePath = "C:\\Users\\Usman\\Documents\\Homework 4\\CourseSiteGenerator\\work\\SiteSaveTest.json";
	JsonObject json = fileMang.loadJSONFile(jsonFilePath);
        
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        JsonObject jsonTA = jsonTAArray.getJsonObject(0);
        String name = jsonTA.getString(JSON_NAME);
        String email = jsonTA.getString(JSON_EMAIL);
         
       assertEquals("Jane Doe",name);
       assertEquals("jane.doe@yahoo.com",email);
         
       JsonObject jsonTA1 = jsonTAArray.getJsonObject(1);
       name = jsonTA1.getString(JSON_NAME);
       email = jsonTA1.getString(JSON_EMAIL);
       
       assertEquals("Joe Shmo", name);
       assertEquals("joe.shmo@yale.com", email);
       
       JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
       JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(0);
       String day = jsonOfficeHours.getString(JSON_DAY);
       String time = jsonOfficeHours.getString(JSON_TIME);
       name = jsonOfficeHours.getString(JSON_NAME);
       
       assertEquals("MONDAY", day);
       assertEquals("9_00am", time);
       assertEquals("Jane Doe", name);
       
       JsonObject jsonOfficeHours1 = jsonOfficeHoursArray.getJsonObject(1);
       day = jsonOfficeHours1.getString(JSON_DAY);
       time = jsonOfficeHours1.getString(JSON_TIME);
       name = jsonOfficeHours1.getString(JSON_NAME);
       
       assertEquals("TUESDAY", day);
       assertEquals("9_00am", time);
       assertEquals("Jane Doe", name);
       
       JsonObject jsonOfficeHours2 = jsonOfficeHoursArray.getJsonObject(2);
       day = jsonOfficeHours2.getString(JSON_DAY);
       time = jsonOfficeHours2.getString(JSON_TIME);
       name = jsonOfficeHours2.getString(JSON_NAME);
       
       assertEquals("WEDNESDAY", day);
       assertEquals("9_00am", time);
       assertEquals("Joe Shmo", name);
       
       JsonObject jsonOfficeHours3 = jsonOfficeHoursArray.getJsonObject(3);
       day = jsonOfficeHours3.getString(JSON_DAY);
       time = jsonOfficeHours3.getString(JSON_TIME);
       name = jsonOfficeHours3.getString(JSON_NAME);
       
       assertEquals("THURSDAY", day);
       assertEquals("9_00am", time);
       assertEquals("Joe Shmo", name);
       
       JsonObject jsonOfficeHours4 = jsonOfficeHoursArray.getJsonObject(4);
       day = jsonOfficeHours4.getString(JSON_DAY);
       time = jsonOfficeHours4.getString(JSON_TIME);
       name = jsonOfficeHours4.getString(JSON_NAME);
       
       assertEquals("FRIDAY", day);
       assertEquals("9_00am", time);
       assertEquals("Joe Shmo", name);
       
       JsonObject jsonOfficeHours5 = jsonOfficeHoursArray.getJsonObject(5);
       day = jsonOfficeHours5.getString(JSON_DAY);
       time = jsonOfficeHours5.getString(JSON_TIME);
       name = jsonOfficeHours5.getString(JSON_NAME);
       
       assertEquals("MONDAY", day);
       assertEquals("9_30am", time);
       assertEquals("Jane Doe", name);
       
       JsonObject jsonOfficeHours6 = jsonOfficeHoursArray.getJsonObject(6);
       day = jsonOfficeHours6.getString(JSON_DAY);
       time = jsonOfficeHours6.getString(JSON_TIME);
       name = jsonOfficeHours6.getString(JSON_NAME);
       
       assertEquals("TUESDAY", day);
       assertEquals("9_30am", time);
       assertEquals("Jane Doe", name);
       
       JsonObject jsonOfficeHours7 = jsonOfficeHoursArray.getJsonObject(7);
       day = jsonOfficeHours7.getString(JSON_DAY);
       time = jsonOfficeHours7.getString(JSON_TIME);
       name = jsonOfficeHours7.getString(JSON_NAME);
       
       assertEquals("WEDNESDAY", day);
       assertEquals("9_30am", time);
       assertEquals("Joe Shmo", name);
       
       JsonObject jsonOfficeHours8 = jsonOfficeHoursArray.getJsonObject(8);
       day = jsonOfficeHours8.getString(JSON_DAY);
       time = jsonOfficeHours8.getString(JSON_TIME);
       name = jsonOfficeHours8.getString(JSON_NAME);
       
       assertEquals("THURSDAY", day);
       assertEquals("9_30am", time);
       assertEquals("Joe Shmo", name);
       
       JsonObject jsonOfficeHours9 = jsonOfficeHoursArray.getJsonObject(9);
       day = jsonOfficeHours9.getString(JSON_DAY);
       time = jsonOfficeHours9.getString(JSON_TIME);
       name = jsonOfficeHours9.getString(JSON_NAME);
       
       assertEquals("FRIDAY", day);
       assertEquals("9_30am", time);
       assertEquals("Joe Shmo", name);
       
       JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATIONS);
       JsonObject jsonRecitations = jsonRecitationArray.getJsonObject(0);
       String section = jsonRecitations.getString(JSON_SECTION);
       String instructor = jsonRecitations.getString(JSON_INSTRUCTOR);
       String dayTime = jsonRecitations.getString(JSON_DAYTIME);
       String location = jsonRecitations.getString(JSON_LOCATION);
       String ta1 = jsonRecitations.getString(JSON_TA1);
       String ta2 = jsonRecitations.getString(JSON_TA2);
       
       assertEquals("R02", section);
       assertEquals("McKenna", instructor);
       assertEquals("Wed 3:30pm-4:23pm",dayTime);
       assertEquals("Old CS 2114",location);
       assertEquals("Jane Doe",ta1);
       assertEquals("Joe Shmo", ta2);
       
       
       JsonObject jsonRecitations1 = jsonRecitationArray.getJsonObject(1);
       section = jsonRecitations1.getString(JSON_SECTION);
       instructor = jsonRecitations1.getString(JSON_INSTRUCTOR);
       dayTime = jsonRecitations1.getString(JSON_DAYTIME);
       location = jsonRecitations1.getString(JSON_LOCATION);
       ta1 = jsonRecitations1.getString(JSON_TA1);
       ta2 = jsonRecitations1.getString(JSON_TA2);
       
       assertEquals("R05", section);
       assertEquals("Banerjee", instructor);
       assertEquals("Tues 5:30pm-6:23pm",dayTime);
       assertEquals("Old CS 2114",location);
       assertEquals("",ta1);
       assertEquals("", ta2);
       
       JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULES);
       JsonObject jsonSchedules = jsonScheduleArray.getJsonObject(0);
       String type = jsonSchedules.getString(JSON_TYPE);
       String date = jsonSchedules.getString(JSON_DATE);
       String title = jsonSchedules.getString(JSON_TITLE);
       String topic = jsonSchedules.getString(JSON_TOPIC);
       String times = jsonSchedules.getString(JSON_TIMES);
       String link = jsonSchedules.getString(JSON_LINK);
       String criteria = jsonSchedules.getString(JSON_CRITERIA);
       
       assertEquals("Holiday",type);
       assertEquals("2/9/2017",date);
       assertEquals("SNOW DAY",title);
       assertEquals("CSE", topic);
       assertEquals("5:00", times );
       assertEquals("www.hello.com", link);
       assertEquals("criteria1", criteria);
       
       JsonObject jsonSchedules1 = jsonScheduleArray.getJsonObject(1);
       type = jsonSchedules1.getString(JSON_TYPE);
       date = jsonSchedules1.getString(JSON_DATE);
       title = jsonSchedules1.getString(JSON_TITLE);
       topic = jsonSchedules1.getString(JSON_TOPIC);
       times = jsonSchedules1.getString(JSON_TIMES);
       link = jsonSchedules1.getString(JSON_LINK);
       criteria = jsonSchedules1.getString(JSON_CRITERIA);
       
       assertEquals("Lecture",type);
       assertEquals("3/15/2017",date);
       assertEquals("Lecture 3",title);
       assertEquals("Math", topic);
       assertEquals("8:00", times );
       assertEquals("www.goodbye.com", link);
       assertEquals("criteria2", criteria);
       
       JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
       JsonObject jsonTeam = jsonTeamArray.getJsonObject(0);
       String namep = jsonTeam.getString(JSON_NAMEP);
       String color = jsonTeam.getString(JSON_COLOR);
       String textColor = jsonTeam.getString(JSON_TEXTCOLOR);
       link = jsonTeam.getString(JSON_LINK);
       
       assertEquals("Atomic Comics", namep);
       assertEquals("552211", color);
       assertEquals("ffffff", textColor);
       assertEquals("http://atomiccomic.com", link);
       
       
       JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
       JsonObject jsonStudent = jsonStudentArray.getJsonObject(0);
       String fName = jsonStudent.getString(JSON_FNAME);
       String lName = jsonStudent.getString(JSON_LNAME);
       String team = jsonStudent.getString(JSON_TEAM);
       String role = jsonStudent.getString(JSON_ROLE);
       
       assertEquals("Beau", fName);
       assertEquals("Brumell", lName);
       assertEquals("Atomic Comics", team);
       assertEquals("Lead Designer", role);
       
       
       
       
       
       
    }
    
   
}
