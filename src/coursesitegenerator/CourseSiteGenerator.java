/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator;
import coursesitegenerator.data.csgData;
import coursesitegenerator.file.csgFiles;
import coursesitegenerator.style.csgStyle;
import coursesitegenerator.workspace.csgWorkspace;
import djf.AppTemplate;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import properties_manager.InvalidXMLFileFormatException;
/**
 *
 * @author Usman
 */
public class CourseSiteGenerator extends AppTemplate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
	try{
        launch(args);}
        catch(Exception e){
            System.out.println(e);
        }
      
        
    }

    @Override
    public void buildAppComponentsHook() {
        try {
            dataComponent = new csgData(this);
        } catch (InvalidXMLFileFormatException ex) {
            Logger.getLogger(CourseSiteGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        workspaceComponent = new csgWorkspace(this);
        fileComponent = new csgFiles(this);
        styleComponent = new csgStyle(this);
    }
    
    public void buildAppComponentsHookTest(){
        fileComponent = new csgFiles(this);
        try {
            dataComponent = new csgData(this);
        } catch (InvalidXMLFileFormatException ex) {
            Logger.getLogger(CourseSiteGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
