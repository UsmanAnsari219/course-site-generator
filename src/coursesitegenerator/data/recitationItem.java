/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Usman
 * @param <E>
 */
public class recitationItem<E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty dayTime;
    private final StringProperty location;
    private final StringProperty TA1;
    private final StringProperty TA2;
    
            
    public recitationItem(String iSection,String initInstructor, String iDayTime, String iLocation, String iTa1,String iTa2) {
    section = new SimpleStringProperty(iSection);
    instructor = new SimpleStringProperty(initInstructor);
    dayTime = new SimpleStringProperty(iDayTime);
    location = new SimpleStringProperty(iLocation);
    TA1 = new SimpleStringProperty(iTa1);
    TA2 = new SimpleStringProperty(iTa2);
    }
    
    public String getSection() {
        return section.get();
    }
    
    public String getInstructor() {
        return instructor.get();
    }
    
    public String getDayTime() {
        return dayTime.get();
    }
    
    public String getLocation() {
        return location.get();
    }
    
    public String getTA1(){
        return TA1.get();
    }
    
    public String getTA2() {
        return TA2.get();
    }
    
    public void setSection(String iSection){
        section.set(iSection);
    }
    
    public void setInstructor(String initInstructor) {
        instructor.set(initInstructor);
    }
    public void setLocation(String iLocation) {
        location.set(iLocation);
    }
    public void setDayTime(String iDayTime) {
        dayTime.set(iDayTime);
    }
    public void setTA1(String iTa1){
        TA1.set(iTa1);
    }
    public void setTA2(String iTa2){
        TA2.set(iTa2);
    }
     @Override
    public int compareTo(E otherIns) {
        return getInstructor().compareTo(((recitationItem)otherIns).getInstructor());
    }
}
