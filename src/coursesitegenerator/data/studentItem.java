/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Usman
 */
public class studentItem {
  
    private final StringProperty fName;
    private final StringProperty lName;
    private final StringProperty teamName;
    private final StringProperty role;
    
    public studentItem(String iFName, String iLName, String iTeam, String iRole) {
        
        fName = new SimpleStringProperty(iFName);
        lName = new SimpleStringProperty(iLName);
        teamName = new SimpleStringProperty(iTeam);
        role = new SimpleStringProperty(iRole);
    }
    
    public String getFName(){
       return fName.get();
    }
    public String getLName() {
        return lName.get();
    }
    public String getTeamName(){
        return teamName.get();
    }
    public String getRole(){
        return role.get();
    }
    public void setFName(String iFName){
        fName.set(iFName);
    }
    public void setLName(String iLName){
        lName.set(iLName);
    }
    public void setTeam(String iTeam){
        teamName.set(iTeam);
    }
    public void setRole(String iRole){
        role.set(iRole);
    }
 }
