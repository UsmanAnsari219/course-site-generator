/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Usman
 */
public class scheduleItem<E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty type;
    private final StringProperty date;
    private final StringProperty title;
    private final StringProperty topic;
    private final StringProperty time;
    private final StringProperty link;
    private final StringProperty criteria;
    
    public scheduleItem(String iType, String iDate, String iTitle, String iTopic, String iTime, String iLink, String iCriteria){
        type = new SimpleStringProperty(iType);
        date = new SimpleStringProperty(iDate);
        title = new SimpleStringProperty(iTitle);
        topic = new SimpleStringProperty(iTopic);
        time = new SimpleStringProperty(iTime);
        link = new SimpleStringProperty(iLink);
        criteria = new SimpleStringProperty(iCriteria);
    }
    
        
     public String getType() {
        return type.get();
    }
    
    public String getDate() {
        return date.get();
    }
    
    public String getTitle() {
        return title.get();
    }
    
    public String getTopic() {
        return topic.get();
    }
    
    public String getTime(){
        return time.get();
    }
    
    public String getLink() {
        return link.get();
    }
    public String getCriteria() {
        return criteria.get();
    }
    public void setType(String iType){
        type.set(iType);
    }
    
    public void setDate(String initDate) {
        date.set(initDate);
    }
    public void setTitle(String iTitle) {
        title.set(iTitle);
    }
    public void setTopic(String iTopic) {
        topic.set(iTopic);
    }
    public void setTime(String iTime){
        time.set(iTime);
    }
    public void setLink(String iLink){
        link.set(iLink);
    }
    public void setCriteria(String iCriteria){
        criteria.set(iCriteria);
    }
    
    
    
    
    @Override
    public int compareTo(E otherType) {
        return getType().compareTo(((scheduleItem)otherType).getType());
    }
    
    
    
}
