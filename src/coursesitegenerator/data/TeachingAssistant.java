/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

/**
 *
 * @author Usman
 */
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E>  {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty email;
    private final BooleanProperty isUndergrad;

    /**
     * Constructor initializes the TA name
     */
    public TeachingAssistant(String initName, String initEmail, Boolean initUnd) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        isUndergrad = new SimpleBooleanProperty(initUnd);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES

    public String getName() {
        return name.get();
    }
    
    public String getEmail() {
        return email.get();
    }
    
    public boolean getUnd() {
        return isUndergrad.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }
    
    public void setEmail(String initEmail) {
        email.set(initEmail);
    }
    
    public void setUnd(Boolean initUnd){
        isUndergrad.set(initUnd);
    }
    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant)otherTA).getName());
    }
    
    @Override
    public String toString() {
        return name.getValue() + email.getValue();
    }
}
