/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.CourseSiteGeneratorProp;
import coursesitegenerator.workspace.csgWorkspace;
import djf.components.AppDataComponent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class csgData implements AppDataComponent {
    CourseSiteGenerator app;
    HashMap<String, StringProperty> officeHours;
    ObservableList<TeachingAssistant> teachingAssistants;
    ArrayList<String> gridHeaders;
    int startHour;
    int endHour;
    
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;
    
    
    
    public csgData(CourseSiteGenerator initApp) {
        app = initApp;

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        teachingAssistants = FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;

        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();

        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(CourseSiteGeneratorProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(CourseSiteGeneratorProp.DAYS_OF_WEEK);
        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
    }

    @Override
    public void resetData() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        officeHours.clear();
    }
    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public ArrayList<String> getGridHeaders() {
        return gridHeaders;
    }

    public ObservableList getTeachingAssistants() {
        return teachingAssistants;
    }

    public String getCellKey(int col, int row) {
        return col + "_" + row;
    }

    public StringProperty getCellTextProperty(int col, int row) {
        String cellKey = getCellKey(col, row);
        return officeHours.get(cellKey);
    }

    public HashMap<String, StringProperty> getOfficeHours() {
        return officeHours;
    }

    public int getNumRows() {
        return ((endHour - startHour) * 2) + 1;
    }

    public String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    public String getCellKey(String day, String time) {
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        if (hour < startHour) {
            milHour += 12;
        }
        row += (milHour - startHour) * 2;
        if (time.contains("_30")) {
            row += 1;
        }
        return getCellKey(col, row);
    }

    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }

    /**
     * This method is for giving this data manager the string property for a
     * given cell.
     */
    public void setCellProperty(int col, int row, StringProperty prop) {
        String cellKey = getCellKey(col, row);
        officeHours.put(cellKey, prop);
    }
    
    public int getSRow(int nStart){
        int row = 1;
        row+= Math.abs(nStart - startHour) *2;
        return row;
    }
    public int getERow(int nEnd, int nStart) {
        int row = 1;
        if(startHour < nStart) {
            row+= (Math.abs(startHour - nEnd)) *2; }
        else {
            row+= (Math.abs(nStart- nEnd)) *2;
        }
        return row;
    }
    
    public void edisaasatTime(int start, int end) {
    HashMap newOfficeHours = new HashMap();
    int sRow = 0;
    int eRow = getERow(end, start);
    int shift = 4;
    csgWorkspace workspace = (csgWorkspace) app.getWorkspaceComponent();
    
    for(sRow = getSRow(start); sRow < eRow; sRow++)
    {
        for(int col = 2; col < 7; col++)
        {
            String cellKey = workspace.buildCellKey(col,sRow);
            StringProperty cellProp = officeHours.get(workspace.buildCellKey(col,sRow));
            newOfficeHours.put(cellKey,cellProp);
        }
    }
    workspace.resetWorkspace();
    int prevStart = startHour;
    initOfficeHours(start,end);
    String cellKey;
    for (sRow = 0; sRow < eRow ; sRow++) {
        for(int col = 2; col < 7; col++){
            try{
                StringProperty cellProp = (StringProperty)newOfficeHours.get(workspace.buildCellKey(col,sRow));
                String name = cellProp.getValue();
                if (start < prevStart) {
                   cellKey = col + "_" + (sRow + shift);
                } else {
                    cellKey = col + "_" + (sRow - shift);
                }
                if(start<prevStart){
                    toggleTAOfficeHoursUpd(cellKey,name);
                }
            }
           catch (NullPointerException noe){}
        }
    }
    
    }   
    public void editTime(int sHour, int eHour) {
        csgWorkspace workspaceComponent = (csgWorkspace) app.getWorkspaceComponent();
        int shift = 0;
        HashMap<String, StringProperty> updateOfficeHours = new HashMap();
        int oldSRow = 1;
        int startR;
        if(sHour > startHour)
        oldSRow = (Math.abs((sHour - startHour))) * 2 +1 ;
        else{
            oldSRow = (Math.abs((sHour - startHour))) +1;
        }
        //int newSRow = 1;

        shift = Math.abs(oldSRow - 1) ;
        int oldHr = startHour;
        int endRow = 1;
        if (sHour > startHour) {
            endRow += (Math.abs(startHour - eHour)) * 2;
        } else {
            endRow += (Math.abs(eHour - startHour)) * 2;
        }

        //int shiftRowIndex = ;
        HashMap tmp = new HashMap(officeHours);

        // if(sHour>startHour && eHour <=endHour) 
        // { //setting boundaries in b/w default hours
        for (int i = oldSRow ; i < endRow; i++) {
            for (int col = 2; col < 7; col++) {

                int row = i;
                String cellKey = col + "_" + row;
                StringProperty cellProp = officeHours.get(cellKey);
                // updateOfficeHours.put(cellKey, cellProp);
                // oldSRow++;
                //newSRow++;
                updateOfficeHours.put(cellKey, cellProp);

                //if(cellProp !=null)
                // toggleTAOfficeHours(cellKey,cellProp.getValue());
            }
            // oldSRow=(newSRow-oldSRow)*2 +1;
            //newSRow= 1;
        }
        // }
        workspaceComponent.resetWorkspace();
        initOfficeHours(sHour, eHour);

        /*   for (HashMap.Entry<String, StringProperty> entry : updateOfficeHours.entrySet()) {
            String key = entry.getKey();
            
            StringProperty prop = updateOfficeHours.get(key);
            String name = prop.getValue();
            toggleTAOfficeHoursUpd(key, name);
        }
         */
        try{ 
        for (int i = oldSRow; i < endRow; i++) {
            for (int col = 2; col < 7; col++) {

                int row = i;
                String cellKey = col + "_" + row;
                StringProperty cellProp = updateOfficeHours.get(cellKey);
                if (sHour < oldHr) {
                    cellKey = col + "_" + (row + shift);
                } else {
                    cellKey = col + "_" + (row - shift);
                }
                // updateOfficeHours.put(cellKey, cellProp);
                // oldSRow++;
                //newSRow++;
                String name = cellProp.getValue();
                toggleTAOfficeHoursUpd(cellKey, name);
                //if(cellProp !=null)
                // toggleTAOfficeHours(cellKey,cellProp.getValue());
            }
            // oldSRow=(newSRow-oldSRow)*2 +1;
            //newSRow= 1;
        }
        //workspaceComponent.reloadOfficeHoursGrid(this);
    }
    catch(NullPointerException noe){}
}
    
    /**
     * This method is for setting the string property for a given cell.
     */
    public void setGridProperty(ArrayList<ArrayList<StringProperty>> grid,
            int column, int row, StringProperty prop) {
        grid.get(row).set(column, prop);
    }

    private void initOfficeHours(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;

        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();

        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        csgWorkspace workspaceComponent = (csgWorkspace) app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }

    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if ((initStartHour >= MIN_START_HOUR)
                && (initEndHour <= MAX_END_HOUR)
                && (initStartHour <= initEndHour)) {
            // THESE ARE VALID HOURS SO KEEP THEM
            initOfficeHours(initStartHour, initEndHour);
        }
    }
    private void toggleTAOfficeHoursUpd(String cellKey, String taName) {
        try{ StringProperty cellProp2 = officeHours.get(cellKey);
        
        String cellText = cellProp2.getValue();
        cellProp2.setValue(cellText + "\n" + taName); }
        catch (NullPointerException noe){}
    }
   
    public boolean containsTA(String testName, String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName) && ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
    }

    public void addTA(String initName) {
        addTA(initName, "");
    }

    public void addTA(String initName, String initEmail) {
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName, initEmail);

        // ADD THE TA
        if (!containsTA(initName, initEmail)) {
            teachingAssistants.add(ta);
        }

        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }

    public boolean checkTA(String cellKey, String taName) {
        StringProperty cellProp;
        cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        String[] lines = cellText.split("/n");

        for (String a : lines) {
            if (a.equals(cellText + "/n")) {
                return true;
            }
        }

        return false;
    }

    public void deleteTA(String initName, String initEmail) {
        TeachingAssistant ta = new TeachingAssistant(initName, "");

        if (ta.getName() == null ? initName == null : ta.getName().equals(initName)) {
            teachingAssistants.remove(ta);
        }
        Collections.sort(teachingAssistants);
    }

    public void addOfficeHoursReservation(String day, String time, String taName) {
        String cellKey = getCellKey(day, time);
        toggleTAOfficeHours(cellKey, taName);
    }

    public void changeTA(String taName, String taEmail) {
        TeachingAssistant ta = new TeachingAssistant(taName, taEmail);
        if ((ta.getName() == null ? taName == null : ta.getName().equals(taName))
                && (ta.getEmail() == null ? taEmail == null : ta.getEmail().equals(taEmail))) {
            teachingAssistants.remove(ta);
            ta.setName(taName);
            ta.setEmail(taEmail);
            teachingAssistants.add(ta);
        }
        Collections.sort(teachingAssistants);

    }

    /**
     * This function toggles the taName in the cell represented by cellKey.
     * Toggle means if it's there it removes it, if it's not there it adds it.
     */
    

    public void toggleTAOfficeHours(String cellKey, String taName) {
        StringProperty cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();
        if (cellText.contains(taName)) {
            removeTAFromCell(cellProp, taName);
        } else {

            cellProp.setValue(cellText + "\n" + taName);
        }
    }

    /**
     * This method removes taName from the office grid cell represented by
     * cellProp.
     */
    public void updateTAInCell(StringProperty cellProp, String taName, String nameText) {

        String cellText = cellProp.getValue();
        String[] text = cellText.split("\n");

        if (cellText.equals(taName)) {
            cellProp.setValue(cellText + nameText);
        } else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n" + 1);
            cellText = cellText.substring(startIndex);
            cellProp.setValue(cellText + "\n" + nameText);
        } else if (cellText.indexOf(taName) < cellText.indexOf("\n", cellText.indexOf(taName))) {
            int startIndex = cellText.indexOf("\n" + taName);
            int endIndex = startIndex + taName.length() + 1;
            cellText = cellText.substring(0, startIndex) + cellText.substring(endIndex);
            cellProp.setValue(cellText + "\n" + nameText);
        } else {
            int startIndex = cellText.indexOf("\n" + taName);
            cellText = cellText.substring(0, startIndex);
            cellProp.setValue(cellText + "\n" + nameText);

        }
    }

    public void removeTAFromCell(StringProperty cellProp, String taName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        String[] text = cellText.split("\n");

        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue("");
        } // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(cellText);
        } // IT MUST BE ANOTHER TA IN THE CELL
        //check if its only ta in cell, 
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            int nameL = taName.length() + 1;
            String u = cellText.substring(0, startIndex);
            String o = cellText.substring(startIndex + nameL);

            int p = o.length();
            String concat = u.concat(o);
            cellProp.setValue(concat);
            System.out.println(o);
            System.out.println(cellText);
            System.out.println(concat);
        }
    }
}
