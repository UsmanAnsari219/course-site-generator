/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Usman
 */
public class courseInfo {
    private final StringProperty subject;
    private final StringProperty semester;
    private final StringProperty title;
    private final StringProperty instrucName;
    private final StringProperty instrucHome;
    private final StringProperty number;
    private final StringProperty year;
    
    public courseInfo( String iSub, String iSem, String iTitle, String iInstrucName, String iInstrucHome, String iNum, String iYear){
        subject = new SimpleStringProperty(iSub);
        semester= new SimpleStringProperty(iSem);
        title = new SimpleStringProperty(iTitle);
        instrucName = new SimpleStringProperty(iInstrucName);
        instrucHome = new SimpleStringProperty(iInstrucHome);
        number = new SimpleStringProperty(iNum);
        year = new SimpleStringProperty(iYear);
        
    }
    
    public String getSubject(){
        return subject.get();
    }
    public String getSemester() {
        return semester.get();
        
    }
    public String getTitle(){
        return title.get();
        
    }
    public String getInstrucName(){
        return instrucName.get();
        
    }
    public String getInstrucHome(){
        return instrucHome.get();
    }
    public String getNumber(){
        return number.get();
        
    }
    public String getYear(){
        return year.get();
    }
}
