/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Usman
 */
public class teamsItem {
    private final StringProperty name;
    private final StringProperty color;
    private final StringProperty textColor;
    private final StringProperty link;
    
    
    public teamsItem(String iName, String iColor, String iTextColor, String iLink) {
        name = new SimpleStringProperty(iName);
        color = new SimpleStringProperty(iColor);
        textColor = new SimpleStringProperty(iTextColor);
        link = new SimpleStringProperty(iLink);
    }
    
    public String getName(){
       return name.get();
    }
    public String getColor(){
        return color.get();
    }
    public String getTextColor(){
        return textColor.get();
        
    }
    public String getLink(){
        return link.get();
    }
    public void setName(String iName){
        name.set(iName);
    }
    public void setColor(String iColor){
        color.set(iColor);
    }
    public void setTextColor(String iTextColor){
        textColor.set(iTextColor);
    }
    public void setLink(String iLink){
        link.set(iLink);
    }
    
}
