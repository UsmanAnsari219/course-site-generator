/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.CourseSiteGeneratorProp;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.csgData;
import coursesitegenerator.style.csgStyle;
import djf.components.AppDataComponent;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import djf.components.AppWorkspaceComponent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;

/**
 *
 * @author Usman
 */
public class tadataWorkspace {
    CourseSiteGenerator app;
    Tab TADataTab;
    HBox tasHeaderBox;
    String tasHeaderText;
    Label tasHeaderLabel;
    TableView taTable;
    csgData data;
    String nameColumnText;
    String emailColumnText;
    String undergradColumnText;
    TableColumn nameColumn;
    TableColumn emailColumn;
    TableColumn undColumn;
    ComboBox<String> startTimeBox;
    ComboBox<String> endTimeBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button clearButton;
    Button updateButton;
    Button changeTimeButton;
    HBox addBox;
    HBox timeBox;
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    HBox TAMang;
    ScrollPane sC;
    Pane works;
    
    csgController controller;
    public tadataWorkspace(CourseSiteGenerator initApp) {
        
        app = initApp;
        controller = new csgController(app);
        TADataTab = new Tab();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        tasHeaderBox = new HBox();
        tasHeaderText = props.getProperty(CourseSiteGeneratorProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        tasHeaderBox.getChildren().add(tasHeaderLabel);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        data = (csgData) app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        taTable.setItems(tableData);
        
        nameColumnText = props.getProperty(CourseSiteGeneratorProp.NAME_COLUMN_TEXT.toString());
        emailColumnText = props.getProperty(CourseSiteGeneratorProp.EMAIL_COLUMN_TEXT.toString());
        undergradColumnText = props.getProperty(CourseSiteGeneratorProp.UND_COLUMN_TEXT.toString());
        nameColumn = new TableColumn(nameColumnText);
        emailColumn = new TableColumn(emailColumnText);
        undColumn = new TableColumn(undergradColumnText);
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("email")
        );
        undColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, Boolean>("undergrad")
        );
        undColumn.setCellFactory(column -> new CheckBoxTableCell());
        
        taTable.getColumns().add(undColumn);
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);
        taTable.setEditable(true);
        
        startTimeBox = new ComboBox();
        endTimeBox = new ComboBox();
        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(CourseSiteGeneratorProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(CourseSiteGeneratorProp.EMAIL_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(CourseSiteGeneratorProp.ADD_BUTTON_TEXT.toString());
        String updateButtonText = props.getProperty(CourseSiteGeneratorProp.UPDATE_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString());
        String comboStartText = props.getProperty(CourseSiteGeneratorProp.COMBO_START_TEXT.toString());
        String comboEndText = props.getProperty(CourseSiteGeneratorProp.COMBO_END_TEXT.toString());
        String changeButtonText = props.getProperty(CourseSiteGeneratorProp.CHANGE_TIMEB_TEXT.toString());
        nameTextField = new TextField();
        emailTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        startTimeBox.setPromptText(comboStartText);
        endTimeBox.setPromptText(comboEndText);
        addButton = new Button(addButtonText);
        changeTimeButton = new Button(changeButtonText);
        addBox = new HBox();
        timeBox = new HBox();
        updateButton = new Button(updateButtonText);
        
        clearButton = new Button(clearButtonText);
        
        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        changeTimeButton.prefWidthProperty().bind(timeBox.widthProperty().multiply(.4));
        //startTimeBox.prefWidthProperty().bind(timeBox.widthProperty().multiply(.2));
        //endTimeBox.prefWidthProperty().bind(timeBox.widthProperty().multiply(.2));
        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        
        addBox.getChildren().add(clearButton);
        addBox.getChildren().add(addButton);
        timeBox.getChildren().add(startTimeBox);
        timeBox.getChildren().add(endTimeBox);
        timeBox.getChildren().add(changeTimeButton);
        
        
        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox = new HBox();
        officeHoursHeaderBox.setMaxWidth(Double.MAX_VALUE);
        
        String officeHoursGridText = props.getProperty(CourseSiteGeneratorProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);
        officeHoursHeaderBox.getChildren().add(timeBox);
        timeBox.setAlignment(Pos.CENTER_RIGHT);
        
        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);   
        taTable.setPrefWidth(4000);
        officeHoursGridPane.setPrefWidth(900);
        leftPane.getChildren().add(taTable);        
        leftPane.getChildren().add(addBox);
        VBox rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);
        sC = new ScrollPane(rightPane);
        sC.setStyle("-fx-background-color: #FFECD7;");
        SplitPane sPane = new SplitPane(leftPane, sC);
        sPane.setStyle("-fx-background-color: #FFECD7;");
        //sPane.setBorder(Border.);
        TAMang = new HBox();
        TAMang.setPrefWidth(9000);
        TAMang.getChildren().add(leftPane);
        TAMang.getChildren().add(sPane);
        
        for(int i = 0; i < 24; i++)
        {
            startTimeBox.getItems().add(buildCellText(i,"00"));
        }
        
        for(int i = 0; i < 24; i++)
        {
            endTimeBox.getItems().add(buildCellText(i,"00"));
        }
        
        
        
        TADataTab.setContent(TAMang);
        TADataTab.setClosable(false);
        
        
        // AND PUT EVERYTHING IN THE WORKSPACE
        //((BorderPane) workspace).setCenter(tabs);

        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH
       

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new csgController(app);
        
        nameTextField.setOnAction(e -> {
            controller.handleAddTA();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
        });
        addButton.setOnAction((ActionEvent e) -> {
            controller.handleAddTA();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            //addButton.setText(addButtonText);
            addSet();
        });
        taTable.setOnKeyPressed(e->{
            if(e.getCode()== KeyCode.BACK_SPACE || e.getCode()== KeyCode.DELETE) {
                controller.handleRemoveTAData();
                
            
            
                app.getGUI().getFileController().markAsEdited(app.getGUI());
                addSet();
            }});
        taTable.setOnMouseReleased(e ->{
            ObservableList<TeachingAssistant> tas = data.getTeachingAssistants();
            updateSet(); 
            controller.handleSelectTA();
             
            
            //controller.handleUpdateTA();
           
            
            
        });
        
        updateButton.setOnAction((ActionEvent e) -> {
            controller.handleUpdateTA();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            //addButton.setText(addButtonText);
            addSet();
        });
        
        clearButton.setOnAction((ActionEvent e) -> {
            controller.handleClearTA();
            addSet();
        });
        changeTimeButton.setOnAction((ActionEvent e) -> {
           String s = startTimeBox.getValue();
           String e1 = endTimeBox.getValue();
           controller.handleTimeChange(s,e1);
            
        });
        
}
    public void updateSet() {
        if(addBox.getChildren().contains(updateButton))
                return;
         addBox.getChildren().remove(addButton);
         addBox.getChildren().add(updateButton);
         
    }
    
    public void addSet()  {
        if(addBox.getChildren().contains(addButton))
            return;
        addBox.getChildren().remove(updateButton);
       
        addBox.getChildren().add(addButton);
    }
    
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }
    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }

    public TableView getTATable() {
        return taTable;
    }
     public HBox getAddBox() {
        return addBox;
    }
    
    public HBox getTimeBox() {
        return timeBox;
    }
    public TextField getNameTextField() {
        return nameTextField;
    }
    
    public TextField getEmailTextField() {
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }
    
    public Button getUpdateButton() {
        return updateButton;
    }
    
    public Button getClearButton() {
        return clearButton;
    }
    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }
    
    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return  officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return  officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        officeHoursGridPane.getChildren().clear();
        
        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }
    
    public void reloadWorkspace(AppDataComponent dataComponent) {
        csgData taData = (csgData)dataComponent;
        reloadOfficeHoursGrid(taData);
    }

    public void reloadOfficeHoursGrid(csgData dataComponent) {        
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }
        
        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));            
        }
        
        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row+1);
            dataComponent.getCellTextProperty(col, row+1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row+1);
            dataComponent.getCellTextProperty(col, row+1).set(buildCellText(endHour+1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row+1);
                col++;
            }
            row += 2;
        }
        
        
        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
                app.getGUI().getFileController().markAsEdited(app.getGUI());
            });
        
        
         p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            }); 
            
    }
        
        
        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        csgStyle csgStyle = (csgStyle)app.getStyleComponent();
        csgStyle.initOfficeHoursGridStyle();
    }
    
    
    
    public void addCellToGrid(csgData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {       
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);
        
        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);
        
        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);
        
        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);
        
        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());        
    }
    
    public Tab getTADataTab() {
       return TADataTab;
    }
}