/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.CourseSiteGeneratorProp;
import coursesitegenerator.data.SitePages;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.csgData;
import coursesitegenerator.style.csgStyle;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.ColumnConstraints;
import static javafx.scene.paint.Color.color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class csgWorkspace extends AppWorkspaceComponent {
    CourseSiteGenerator app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    csgController controller;
    coursesitegenerator.workspace.courseWorkspace work1;
    coursesitegenerator.workspace.recitationWorkspace work2;
    coursesitegenerator.workspace.tadataWorkspace work3;
    coursesitegenerator.workspace.projectWorkspace work4;
    coursesitegenerator.workspace.scheduleWorkspace work5;
    Tab courseDetails;
    Tab recitationTab;
    Tab taDataTab;
    Tab projectsTab;
    Tab scheduleTab;
    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    Label tasHeaderLabel;
    
    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant, String> nameColumn;
    TableColumn<TeachingAssistant, String> emailColumn;
    TableColumn<TeachingAssistant, Boolean> undColumn;
    TableView<SitePages> sitePages;

    // THE TA INPUT
    HBox addBox;
    HBox timeBox;
    ComboBox<String> startTimeBox;
    ComboBox<String> endTimeBox;
    TextField nameTextField;
    TextField emailTextField;
    TextField sectionText;
    TextField instrucText;
    TextField daytimeText;
    TextField locationText;
    ComboBox<TeachingAssistant> supTA1;
    ComboBox<TeachingAssistant> supTA2;
    GridPane recP1;
    VBox recP1b;
    VBox recBox;
    VBox recBoxb;
    TableView recTable;
    VBox recTableBox;
    GridPane couP1;
    Pane couP2;
    VBox couV1;
    VBox couV2;
    VBox couV2b;
    HBox couH1;
    HBox couH2;
    HBox couH3;
    HBox couH4;
    VBox couV3;
    VBox couV3b;
    VBox couVOG;
    
    HBox addHBox;
    VBox sched;
    VBox schedb;
    VBox schedbb;
    VBox calBound;
    HBox calDates;
    VBox calDatesb;
    VBox schedItems;
    VBox schedItemsb;
    TableView schedItems1;
    GridPane schedAdd;
    
    Button couChange1;
    Button couChange2;
    Button couChange3;
    Button recAdd;
    Button recClear;
    Button sTemp;
    Button couChange4;
    Button deleteRow;
    ComboBox subjCom;
    ComboBox semCom;
    ComboBox numCom;
    ComboBox yearCom;
    TextField titleText = new TextField();
    TextField insnamText = new TextField();
    TextField inshomeText = new TextField();
    ComboBox styleSh;
    Label recL;
    
    DatePicker monText;
    DatePicker friText;
    ComboBox typeCombo;
    DatePicker datePick;
    TextField timeText;
    TextField schTitleText;
    TextField topicText;
    TextField linkText;
    TextField critText;
   
    ColorPicker color;
    ColorPicker textColor;
    VBox projs;
    VBox projs1;
    VBox projs2;
    TableView teams;
    GridPane teamsAE;
    TableView studs;
    GridPane students;
    TextField namePText;
    TextField linkPText;
    TextField fnamePText;
    TextField lnamePText;
    ComboBox teamPCombo;
    TextField rolePText;
    Button auP1;
    Button cP1;
    Button auP2;
    Button cP2;
    CheckBox und;
            
    Button addButton;
    Button updateButton;
    Button clearButton;
    Button changeTimeButton;
    Button addupButton;
    TabPane tabs;
    
    //Tab TADataTab;
    Tab RecitationData;
    Tab ScheduleData;
    Tab ProjectData;
    TextField testField;
    VBox RecTab;
    

    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    
    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    
    public csgWorkspace(CourseSiteGenerator initApp) {
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        tabs = new TabPane();
        //CourseDetails = new Tab();
        work1 = new courseWorkspace(app);
        work2 = new recitationWorkspace(app);
        work3 = new tadataWorkspace(app);
        work4 = new projectWorkspace(app);
        work5 = new scheduleWorkspace(app);
        courseDetails = work1.getCourseDetails();
        recitationTab = work2.getRecitationTab();
        taDataTab = work3.getTADataTab();
        projectsTab = work4.getProjectTab();
        scheduleTab = work5.getScheduleTab();
        
        
        
        deleteRow = new Button();
        deleteRow.setText(props.getProperty(CourseSiteGeneratorProp.DELETEROW.toString()));
        
        Label addE = new Label();
        addE.setText(props.getProperty(CourseSiteGeneratorProp.AE_TEXT.toString()));
         Label ae = new Label();
        ae.setText(props.getProperty(CourseSiteGeneratorProp.AE_TEXT.toString()));
        
       
        
        taDataTab.setText(props.getProperty(CourseSiteGeneratorProp.TADATATAB_TEXT.toString()));
        recitationTab.setText(props.getProperty(CourseSiteGeneratorProp.RECITATIONTAB_TEXT.toString()));
        courseDetails.setText(props.getProperty(CourseSiteGeneratorProp.COURSETAB_TEXT.toString()));
        scheduleTab.setText(props.getProperty(CourseSiteGeneratorProp.SCHEDULETAB_TEXT.toString()));
        projectsTab.setText(props.getProperty(CourseSiteGeneratorProp.PROJECTTAB_TEXT.toString()));
        
        
        
        
        
        
        tabs.getTabs().add(courseDetails);
        tabs.getTabs().add(taDataTab);
        tabs.getTabs().add(recitationTab);
        tabs.getTabs().add(scheduleTab);
        tabs.getTabs().add(projectsTab);
        
        /*
        for(int i = 0; i < 24; i++)
        {
            startTimeBox.getItems().add(buildCellText(i,"00"));
        }
        
        for(int i = 0; i < 24; i++)
        {
            endTimeBox.getItems().add(buildCellText(i,"00"));
        } */
        
        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        
        workspace = new BorderPane();
        taTable = work3.getTATable();
        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(tabs);
       
        
       taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.9));
        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new csgController(app);
        
        /*
        // CONTROLS FOR ADDING TAs
        nameTextField.setOnAction(e -> {
            controller.handleAddTA();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
        });
        addButton.setOnAction((ActionEvent e) -> {
            controller.handleAddTA();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            //addButton.setText(addButtonText);
            addSet();
        });
        taTable.setOnKeyPressed(e->{
            if(e.getCode()== KeyCode.BACK_SPACE || e.getCode()== KeyCode.DELETE) {
                controller.handleRemoveTAData();
                
            
            
                app.getGUI().getFileController().markAsEdited(app.getGUI());
                addSet();
            }});
        taTable.getSelectionModel().selectedItemProperty().addListener(e ->{
            controller.handleSelectTA();
            //controller.handleUpdateTA();
            updateSet();
        });
        
        updateButton.setOnAction((ActionEvent e) -> {
            controller.handleUpdateTA();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            //addButton.setText(addButtonText);
            addSet();
        });
        
        clearButton.setOnAction((ActionEvent e) -> {
            controller.handleClearTA();
            addSet();
        });
        changeTimeButton.setOnAction((ActionEvent e) -> {
           String s = startTimeBox.getValue();
           String e1 = endTimeBox.getValue();
            controller.handleTimeChange(s,e1);
            
        });
        
        workspace.setOnKeyPressed((KeyEvent e) -> {
            if((e.getCode()==(KeyCode.Z)) && e.isControlDown()){
                controller.handleUndo();
                System.out.println("undolisterner");
            }
            if((e.getCode() == (KeyCode.Y))  && e.isControlDown()) {
            controller.handleRedo();
            System.out.println("redolistener");
            }
        }); */
    }

//    public void updateSet() {
//        if(addBox.getChildren().contains(updateButton))
//                return;
//         addBox.getChildren().remove(addButton);
//         addBox.getChildren().add(updateButton);
//         
//    }
//    
//    public void addSet()  {
//        if(addBox.getChildren().contains(addButton))
//            return;
//        addBox.getChildren().remove(updateButton);
//       
//        addBox.getChildren().add(addButton);
//    }
    
    
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    
    
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }
   
    public TabPane getTabs(){
        return tabs;
    }
    
    
    
    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }
    
    

    public HBox getAddBox() {
        return addBox;
    }
    
    public HBox getTimeBox() {
        return timeBox;
    }
    public TextField getNameTextField() {
        return nameTextField;
    }
    
    public TextField getEmailTextField() {
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }
    
    public Pane getWork() {
        return workspace;
    }
    public courseWorkspace getCourseWorkspace(){
        return work1;
    }
    
     public recitationWorkspace getRecitationWorkspace(){
        return work2;
    }
    
    public tadataWorkspace getTADataWorkspace() {
        return work3;
    }
    
    public projectWorkspace getProjectsWorkspace() {
        return work4;
    }
    
    public scheduleWorkspace getScheduleWorkspace() {
        return work5;
    }   
    
    /*
    public Button getUpdateButton() {
        return updateButton;
    }
    
    public Button getClearButton() {
        return clearButton;
    }
    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }
    
    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    } */
    

    @Override
    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        work3.resetWorkspace();
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        csgData taData = (csgData)dataComponent;
        work3.reloadOfficeHoursGrid(taData);
        
    }

    
}
