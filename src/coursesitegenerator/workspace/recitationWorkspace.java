/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.CourseSiteGeneratorProp;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.recitationItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class recitationWorkspace {
    csgData data;
    recitationController controller;
    CourseSiteGenerator app;
    Tab RecitationTab;
    GridPane recP1;
    VBox recP1b;
    VBox recBoxb;
    VBox recBox;
    VBox recTableBox;
    Label recL;
    TableView recTable;
    TableColumn section;
    TableColumn instructor;
    TableColumn dayTime;
    TableColumn location;
    TableColumn ta1;
    TableColumn ta2;
    String sectionPromptText;
    String instrPromptText;
    String dayPromptText;
    String locationPromptText;
    String ta1PromptText;
    String ta2PromptText;
    TextField sectionText;
    TextField   instrucText;
    TextField   daytimeText;
    TextField   locationText;
    ComboBox   supTA1;
    ComboBox   supTA2;
    Button    recAdd;
    Button recUpdate;
    Button    recClear;
    ObservableList<TeachingAssistant> tas;
    public recitationWorkspace(CourseSiteGenerator initApp) {
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        controller = new recitationController(app);
        RecitationTab = new Tab();
        recP1 = new GridPane();
        recP1b = new VBox();
        recBox = new VBox();
        recBoxb = new VBox();
        recL = new Label(props.getProperty(CourseSiteGeneratorProp.REC_TEXT.toString()));
        recTable = new TableView();
        recTableBox = new VBox();
        section = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SECC_TEXT.toString()));
        instructor = new TableColumn(props.getProperty(CourseSiteGeneratorProp.INSC_TEXT.toString()));
        dayTime= new TableColumn(props.getProperty(CourseSiteGeneratorProp.DAYC_TEXT.toString()));
        location = new TableColumn(props.getProperty(CourseSiteGeneratorProp.LOCC_TEXT.toString()));
        ta1 = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TACC_TEXT.toString()));
        ta2 = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TACC_TEXT.toString()));
        tas = FXCollections.observableArrayList();
        
        sectionPromptText = props.getProperty(CourseSiteGeneratorProp.SECTION_PROMPT_TEXT.toString());
        instrPromptText = props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_PROMPT_TEXT.toString());
        dayPromptText = props.getProperty(CourseSiteGeneratorProp.DAYTIME_PROMPT_TEXT.toString());
        locationPromptText = props.getProperty(CourseSiteGeneratorProp.LOCATION_PROMPT_TEXT.toString());
        ta1PromptText = props.getProperty(CourseSiteGeneratorProp.SUPTA1_PROMPT_TEXT.toString());
        ta2PromptText = props.getProperty(CourseSiteGeneratorProp.SUPTA1_PROMPT_TEXT.toString());
        
        
        Label addE = new Label();
        addE.setText(props.getProperty(CourseSiteGeneratorProp.AE_TEXT.toString()));
        
        //this creates prop error
        sectionText = new TextField();
        instrucText = new TextField();
        daytimeText = new TextField();
        locationText = new TextField();
        supTA1 = new ComboBox<>();
        supTA2 = new ComboBox<>();
        recAdd = new Button();
        recUpdate = new Button();
        recClear = new Button();
        sectionText.setPromptText(sectionPromptText);
        instrucText.setPromptText(instrPromptText);
        daytimeText.setPromptText(dayPromptText);
        locationText.setPromptText(locationPromptText);
        supTA1.setPromptText(ta1PromptText);
        supTA2.setPromptText(ta2PromptText);
        
        data = (csgData) app.getDataComponent();
        ObservableList<recitationItem> recData = data.getRecitations();
        recTable.setItems(recData);
        
        section.setCellValueFactory(
                new PropertyValueFactory<recitationItem, String>("section")
        );
        instructor.setCellValueFactory(
                new PropertyValueFactory<recitationItem, String>("instructor")
        );
        dayTime.setCellValueFactory(
                new PropertyValueFactory<recitationItem, String>("dayTime")
        );
        location.setCellValueFactory(
                new PropertyValueFactory<recitationItem, String>("location")
        );
        ta1.setCellValueFactory(
                new PropertyValueFactory<recitationItem, String>("TA1")
        );
        ta2.setCellValueFactory(
                new PropertyValueFactory<recitationItem, String>("TA2")
        );
        
        
        for(int i = 0; i <tas.size();i++){
            supTA1.getItems().add(tas.get(i));
            supTA2.getItems().add(tas.get(i));
        }
        
        
        
        recTable.getColumns().add(section);
        recTable.getColumns().add(instructor);
        recTable.getColumns().add(dayTime);
        recTable.getColumns().add(location);
        recTable.getColumns().add(ta1);
        recTable.getColumns().add(ta2);
        
        //Rectangle recRec = new Rectangle();
        Label ae = new Label();
        ae.setText(props.getProperty(CourseSiteGeneratorProp.AE_TEXT.toString()));
        Label sec = new Label();
        sec.setText(props.getProperty(CourseSiteGeneratorProp.SEC_TEXT.toString()));
        Label ins = new Label();
        ins.setText(props.getProperty(CourseSiteGeneratorProp.INS_TEXT.toString()));
        Label day = new Label();
        day.setText(props.getProperty(CourseSiteGeneratorProp.DAY_TEXT.toString()));
        Label loc = new Label();
        loc.setText(props.getProperty(CourseSiteGeneratorProp.LOC_TEXT.toString()));
        Label sup1 = new Label();
        sup1.setText(props.getProperty(CourseSiteGeneratorProp.SUP1_TEXT.toString()));
        Label sup2 = new Label();
        sup2.setText(props.getProperty(CourseSiteGeneratorProp.SUP1_TEXT.toString()));
        recClear.setText(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));
        recAdd.setText(props.getProperty(CourseSiteGeneratorProp.ADD_BUTTON_TEXT.toString()));
        recUpdate.setText(props.getProperty(CourseSiteGeneratorProp.UPDATE_BUTTON_TEXT.toString()));
        
        
        
        recP1 = new GridPane();
        recP1.add(ae, 0, 0);
        recP1.add(sec, 1, 2);
        recP1.add(ins, 1, 3);
        recP1.add(day, 1, 4);
        recP1.add(loc, 1, 5);
        recP1.add(sup1, 1, 6);
        recP1.add(sup2, 1, 7);
        recP1.add(sectionText, 3, 2 );
        recP1.add(instrucText, 3, 3);
        recP1.add(daytimeText, 3, 4);
        recP1.add(locationText, 3, 5);
        recP1.add(supTA1, 3, 6);
        recP1.add(supTA2, 3, 7);
        recP1.add(recAdd, 1, 8);
        recAdd.setPadding(new Insets(3,10,3,10));
        recP1.add(recClear, 2, 8);
        recClear.setPadding(new Insets(3,10,3,10));
        recUpdate.setPadding(new Insets(3,10,3,10));
        
        recBox.getChildren().add(recL);
        recL.getStyleClass().add("titles");
        recL.setPadding(new Insets(10, 10, 10, 10));
        recTableBox.getChildren().add(recTable);
        recBox.getChildren().add(recTableBox);
        
        recP1b.getChildren().add(recP1);
        recBox.getChildren().add(recP1b);
        recBoxb.getChildren().add(recBox);
        
        RecitationTab.setContent(recBoxb);
        RecitationTab.setClosable(false);
        
        recTable.setOnMouseReleased(e ->{
            controller.handleSelectRec();
            updateSet();
        });
        recTable.setOnKeyPressed(e->{
            if(e.getCode()== KeyCode.BACK_SPACE || e.getCode()== KeyCode.DELETE) {
                controller.handleRemoveRec();
                app.getGUI().getFileController().markAsEdited(app.getGUI());
        }});
        recAdd.setOnAction((ActionEvent e) -> {
            controller.handleAddRec();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
        });
        recClear.setOnAction((ActionEvent e) -> {
            controller.handleClearRec();
            addSet();
        });
        recUpdate.setOnAction((ActionEvent e) -> {
            controller.handleUpdateRec();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            addSet();
        });
       
    }
    
    public GridPane getRecP1(){
        return recP1;
    }
    public VBox getRecP1b(){
        return recP1b;
    }
    
    public VBox getRecTableBox(){
        return recTableBox;
    }
    public TableView getRecTable(){
        return recTable;
    }
    public Label getRec(){
        
        return recL;
    }
    
    public TextField getSectionTextField(){
        return sectionText;
    }
    public TextField getInstructorTextField(){
        return instrucText;
    }
    public TextField getDayTimeTextField(){
        return daytimeText;
    }
    public TextField getLocationTextField(){
        return locationText;
    }
    public ComboBox getTA1Box(){
        return supTA1;
    }
    public ComboBox getTA2Box(){
        return supTA2;
    }
    public VBox getRecBoxb(){
        return recBoxb;
    }
    public VBox getRecBox(){
        return recBox;
    }
    public Tab getRecitationTab(){
        return RecitationTab;
    }
    public void updateSet() {
        if(recP1.getChildren().contains(recUpdate))
                return;
         recP1.getChildren().remove(recAdd);
         recP1.add(recUpdate, 1, 8);
         
    }
    
    public void addSet()  {
        if(recP1.getChildren().contains(recAdd))
            return;
        recP1.getChildren().remove(recUpdate);
       
        recP1.add(recAdd, 1, 8);
    }
}
