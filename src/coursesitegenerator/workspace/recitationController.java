/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.recitationItem;
import djf.ui.AppMessageDialogSingleton;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class recitationController {
    CourseSiteGenerator app;
    
    
    public recitationController(CourseSiteGenerator initApp){
        app = initApp;
    }

   
    
    public void handleSelectRec()
    {
        
             csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
             recitationWorkspace recWork = workspace.getRecitationWorkspace();
        
             TableView recTable = recWork.getRecTable();
             Object selectedItem = recTable.getSelectionModel().getSelectedItem();
             
             recitationItem rec = (recitationItem)selectedItem;
             if( !(selectedItem == null)) {
            
                    TextField secTextField = recWork.getSectionTextField();
                    TextField insTextField = recWork.getInstructorTextField();
                    TextField dayTextField = recWork.getDayTimeTextField();
                    TextField locTextField = recWork.getLocationTextField();
                    ComboBox TA1 = recWork.getTA1Box();
                    ComboBox TA2 = recWork.getTA2Box();
  
                    String section = rec.getSection();
                    String instructor = rec.getInstructor();
                    String dayTime = rec.getDayTime();
                    String location = rec.getLocation();
                    String ta1 = rec.getTA1();
                    String ta2 = rec.getTA2();
                    
                    secTextField.setText(section);
                    insTextField.setText(instructor);
                    dayTextField.setText(dayTime);
                    locTextField.setText(location);
                    try{
                    for(int i = 0; i<TA1.getItems().size(); i++){
                    if(ta1.equals(TA1.getItems().get(i))){
                        System.out.println("got here");
                        TA1.getSelectionModel().select(i);
                        }
                    }
                    for(int i = 0; i<TA2.getItems().size(); i++){
                    if(ta2.equals(TA2.getItems().get(i))){
                        TA2.getSelectionModel().select(i);
                        }
                    }
                    }
                    catch(NullPointerException e){
                        System.out.println("nah");
                    }
             } 
             else
                 System.out.println("Nothing selected");
             //ta2.equals(TA2.getItems().get(i))
    }


    public void handleAddRec(){
    csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
    recitationWorkspace recWork = workspace.getRecitationWorkspace();
    
    csgData data = (csgData)app.getDataComponent();
    ObservableList<TeachingAssistant> recTA = data.getTeachingAssistants();
    
    TextField secTextField = recWork.getSectionTextField();
    String section = secTextField.getText();
    TextField insTextField = recWork.getInstructorTextField();
    String instructor = insTextField.getText();
    TextField dayTextField = recWork.getDayTimeTextField();
    String day = dayTextField.getText();
    TextField locationTextField = recWork.getLocationTextField();
    String location = locationTextField.getText();
    ComboBox ta1Box = recWork.getTA1Box();
    ComboBox ta2Box = recWork.getTA2Box();
    String ta1 = (String) ta1Box.getSelectionModel().getSelectedItem();
    String ta2 = (String) ta2Box.getSelectionModel().getSelectedItem();
    // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
    
    // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
    PropertiesManager props = PropertiesManager.getPropertiesManager();

        data.addRec(section, instructor, day, location, ta1, ta2 );

        
        // CLEAR THE TEXT FIELDS
        secTextField.setText("");
        insTextField.setText("");
        dayTextField.setText("");
        locationTextField.setText("");
        ta1Box.getSelectionModel().clearSelection();
        ta2Box.getSelectionModel().clearSelection();
        
        
    }
    
    public void handleRemoveRec()
    {
        try{
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
       
        TableView recTable = workspace.getRecitationWorkspace().getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        recitationItem rec = (recitationItem)selectedItem;
        
        csgData data = (csgData)app.getDataComponent();
        ObservableList<recitationItem> recTableData = data.getRecitations();
        //String taName = ta.getName();
        recTableData.remove(rec);
        }
        
        catch(NullPointerException e)
        {
            System.err.println("Cannot delete when no TA in table, would cause exception: " + e);
        }
    
    }
    
    public void handleClearRec(){
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        recitationWorkspace recWork = workspace.getRecitationWorkspace();
        TableView recTable = recWork.getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        recitationItem rec = (recitationItem)selectedItem;
        TextField secTextField = recWork.getSectionTextField();
        TextField insTextField = recWork.getInstructorTextField();
        TextField dayTextField = recWork.getDayTimeTextField();
        TextField locationTextField = recWork.getLocationTextField();
        ComboBox supTA1 = recWork.getTA1Box();
        ComboBox supTA2 = recWork.getTA2Box();
        
        try{
        String section = secTextField.getText();
        String instructor = insTextField.getText();
        String day = dayTextField.getText();
        String location = locationTextField.getText();
        String ta1 = supTA1.getValue().toString();
        String ta2 = supTA2.getValue().toString();

        secTextField.setText("");
        insTextField.setText("");
        dayTextField.setText("");
        locationTextField.setText("");
        supTA1.getSelectionModel().clearSelection();
        supTA2.getSelectionModel().clearSelection();
        
        //jTPS_Transaction transaction = (jTPS_Transaction) new clearTrans(nameTextField, emailTextField, name, email);
        //jTPS.addTransaction(transaction);
        
        }catch(NullPointerException noe) {
        secTextField.requestFocus(); }
    }
    
    public void handleUpdateRec(){
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
       recitationWorkspace recWork = workspace.getRecitationWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TableView recTable = recWork.getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        csgData data = (csgData)app.getDataComponent();
        ObservableList<recitationItem> tableData = data.getRecitations();
        int selIndex = tableData.indexOf(selectedItem);
        try{
        recitationItem rec = (recitationItem)selectedItem;
        
        String sec = rec.getSection();
        String ins = rec.getInstructor();
        String day = rec.getDayTime();
        String loc = rec.getLocation();
        
        TextField secTextField = recWork.getSectionTextField();
        TextField insTextField = recWork.getInstructorTextField();
        TextField dayTextField = recWork.getDayTimeTextField();
        TextField locTextField = recWork.getLocationTextField();
        ComboBox ta1Box = recWork.getTA1Box();
        ComboBox ta2Box = recWork.getTA2Box();
  
        String section = secTextField.getText();
        String instruc = insTextField.getText();
        String dayTime = dayTextField.getText();
        String location = locTextField.getText();
        String ta1 = (String) ta1Box.getSelectionModel().getSelectedItem();
        String ta2 = (String) ta2Box.getSelectionModel().getSelectedItem();
        
        
//       
//         if (name.isEmpty()) {
//	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
//        }
//        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
//        else if (name.equals(taName) && email.contains(taEmail)) {
//	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
//        }
//        else if(!validEmail(email)) {
//            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//            dialog.show(props.getProperty(INCORRECT_TA_EFORMAT_TITLE), props.getProperty(INCORRECT_TA_EFORMAT_MESSAGE));
//        }
        
        //else{
        rec.setSection(section);
        rec.setInstructor(instruc);
        rec.setDayTime(dayTime);
        rec.setLocation(location);
        rec.setTA1(ta1);
        rec.setTA2(ta2);
        tableData.set(selIndex, rec);
        
        secTextField.setText("");
        insTextField.setText("");
        dayTextField.setText("");
        locTextField.setText("");
        ta1Box.getSelectionModel().clearSelection();
        ta2Box.getSelectionModel().clearSelection();
        
        secTextField.requestFocus();
        
        //workspace.reloadOfficeHoursGrid(data);
        //};
        }
        catch(NullPointerException e){
            System.err.println("Cannot select when no TA in table, would cause exception: " + e);
        }
        
    }
}
    
    
    


