/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_CRIT_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_CRIT_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_DATE_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_DATE_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_LINK_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_LINK_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TIME_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TIME_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TITLE_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TITLE_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TOPIC_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TOPIC_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TYPE_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_SCH_TYPE_TITLE;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.scheduleItem;
import djf.ui.AppMessageDialogSingleton;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Locale;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.converter.DateTimeStringConverter;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class scheduleController {
    CourseSiteGenerator app;
    
    public scheduleController(CourseSiteGenerator initApp){
        app = initApp;
    }
    
    public void handleSelectSch()
    {
     
      csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
      scheduleWorkspace schedWork = workspace.getScheduleWorkspace();
     
      TableView schedTable = schedWork.getSchedItemsTable();
      Object selectedItem = schedTable.getSelectionModel().getSelectedItem();
      
      scheduleItem sched = (scheduleItem)selectedItem;
      if( !(selectedItem == null)) {
     
             TextField timeTextField = schedWork.getTimeTextField();
             TextField titleTextField = schedWork.getTitleTextField();
             TextField topicTextField = schedWork.getTopicTextField();
             TextField linkTextField = schedWork.getLinkTextField();
             TextField critTextField = schedWork.getCriteriaTextField();
             ComboBox typeBox = schedWork.getTypeCombo();
             DatePicker datePick = schedWork.getDatePick();
             try{
             DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
             formatter = formatter.withLocale(Locale.ITALY);
     
             
             String time = sched.getTime();
             String title = sched.getTitle();
             String topic = sched.getTopic();
             String link = sched.getLink();
             String crit = sched.getCriteria();
             String datep = sched.getDate();
             LocalDate date = LocalDate.parse(datep, formatter);
             String type = sched.getType();
             
             
             timeTextField.setText(time);
             titleTextField.setText(title);
             topicTextField.setText(topic);
             linkTextField.setText(link);
             critTextField.setText(crit);
             datePick.setValue(date);
             typeBox.setValue(type);
             }
           catch(DateTimeParseException e) {}
      } 
      else
          System.out.println("Nothing selected");
      //ta2.equals(TA2.getItems().get(i))
    }
    
    public void handleAddSch(){
    csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
    scheduleWorkspace schWork = workspace.getScheduleWorkspace();
    PropertiesManager props = PropertiesManager.getPropertiesManager();
    
    csgData data = (csgData)app.getDataComponent();
    ObservableList<scheduleItem> schedules = data.getSchedules();
    try{
    ComboBox typeBox = schWork.getTypeCombo();
    DatePicker dateText = schWork.getDatePick();
    TextField titleTextField = schWork.getTitleTextField();
    String title = titleTextField.getText();
    TextField topicTextField = schWork.getTopicTextField();
    String topic = topicTextField.getText();
    TextField timeTextField = schWork.getTimeTextField();
    String time = timeTextField.getText();
    TextField linkTextField = schWork.getLinkTextField();
    String link = linkTextField.getText();
    TextField critTextField = schWork.getCriteriaTextField();
    String criteria = critTextField.getText();
    String type = (String) typeBox.getSelectionModel().getSelectedItem();
    String date = dateText.getValue().toString();
    
        if (type == null || type.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_TYPE_TITLE), props.getProperty(MISSING_SCH_TYPE_MESSAGE));            
        }
        if (date == null || date.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_DATE_TITLE), props.getProperty(MISSING_SCH_DATE_MESSAGE));            
        }
        if (time == null || time.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_TIME_TITLE), props.getProperty(MISSING_SCH_TIME_MESSAGE));            
        }
        if (topic == null ||topic.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_TOPIC_TITLE), props.getProperty(MISSING_SCH_TOPIC_MESSAGE));            
        }
        if (link == null || link.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_LINK_TITLE), props.getProperty(MISSING_SCH_LINK_MESSAGE));            
        }
        if(title == null || title.isEmpty()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_TITLE_TITLE), props.getProperty(MISSING_SCH_TITLE_MESSAGE));  
        }
        if (criteria == null || criteria.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_CRIT_TITLE), props.getProperty(MISSING_SCH_CRIT_MESSAGE));            
        }
    // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
  
    
    // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
   
        else{
        data.addSch(type, date, title, topic, time, link, criteria);
        }
   

        
        // CLEAR THE TEXT FIELDS
        timeTextField.setText("");
        titleTextField.setText("");
        topicTextField.setText("");
        linkTextField.setText("");
        critTextField.setText("");
        typeBox.getSelectionModel().clearSelection();
    }
    catch(NullPointerException e){}
        //dateText
        
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
       // nameTextField.requestFocus();
        
        // WE'VE CHANGED STUFF
        //markWorkAsEdited();
        //}
    }
    
    public void handleRemoveSch()
    {
        try{
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
       
        TableView schTable = workspace.getScheduleWorkspace().getSchedItemsTable();
        Object selectedItem = schTable.getSelectionModel().getSelectedItem();
        scheduleItem sch = (scheduleItem)selectedItem;
        
        csgData data = (csgData)app.getDataComponent();
        ObservableList<scheduleItem> schTableData = data.getSchedules();
        
        schTableData.remove(sch);
        }
        
        catch(NullPointerException e)
        {
            System.err.println("Cannot delete when no TA in table, would cause exception: " + e);
        }
    
    }
    
     public void handleClearSch(){
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        scheduleWorkspace schWork = workspace.getScheduleWorkspace();
        TableView schTable = schWork.getSchedItemsTable();
        Object selectedItem = schTable.getSelectionModel().getSelectedItem();
        scheduleItem sch = (scheduleItem)selectedItem;
        TextField timeTextField = schWork.getTimeTextField();
        TextField titleTextField = schWork.getTitleTextField();
        TextField topicTextField = schWork.getTopicTextField();
        TextField linkTextField = schWork.getLinkTextField();
        TextField critTextField = schWork.getCriteriaTextField();
        ComboBox typeBox = schWork.getTypeCombo();
        DatePicker datep = schWork.getDatePick();
        
        try{
        String time = timeTextField.getText();
        String title = titleTextField.getText();
        String topic = topicTextField.getText();
        String link = linkTextField.getText();
        String crit = critTextField.getText();
        String type = typeBox.getValue().toString();
        String date = datep.getValue().toString();

        timeTextField.setText("");
        titleTextField.setText("");
        topicTextField.setText("");
        linkTextField.setText("");
        critTextField.setText("");
        typeBox.getSelectionModel().clearSelection();
        //datep.getEditor().clear();
        
        
        //jTPS_Transaction transaction = (jTPS_Transaction) new clearTrans(nameTextField, emailTextField, name, email);
        //jTPS.addTransaction(transaction);
        
        }catch(NullPointerException noe) {
        timeTextField.requestFocus(); }
    }
        public void handleUpdateSch(){
         csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
         scheduleWorkspace schWork = workspace.getScheduleWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TableView schTable = schWork.getSchedItemsTable();
        Object selectedItem = schTable.getSelectionModel().getSelectedItem();
        csgData data = (csgData)app.getDataComponent();
       
        ObservableList<scheduleItem> tableData = data.getSchedules();
        int selIndex = tableData.indexOf(selectedItem);
        
        scheduleItem sch = (scheduleItem)selectedItem;
        
        String timee = sch.getTime();
        String titlee = sch.getTitle();
        String topicc = sch.getTopic();
        String linkk = sch.getLink();
        String critt = sch.getCriteria();
        
        TextField timeTextField = schWork.getTimeTextField();
        TextField titleTextField = schWork.getTitleTextField();
        TextField topicTextField = schWork.getTopicTextField();
        TextField linkTextField = schWork.getLinkTextField();
        TextField critTextField = schWork.getCriteriaTextField();
        ComboBox typeCombo = schWork.getTypeCombo();
        DatePicker datep = schWork.getDatePick();
        String a = datep.getValue().toString();
        final DatePicker datePicker = new DatePicker(LocalDate.parse(a));
        LocalDate date1 = datePicker.getValue();
        DateFormat oDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //String szDate = oDateFormat.format(date1);
       
        try{
        String time = timeTextField.getText();
        String title = titleTextField.getText();
        String topic = topicTextField.getText();
        String link = linkTextField.getText();
        String crit = critTextField.getText();
        
        String type = typeCombo.getValue().toString();
        String date = date1.toString();
        
        if (type == null || type.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_TYPE_TITLE), props.getProperty(MISSING_SCH_TYPE_MESSAGE));            
        }
        if (date.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_DATE_TITLE), props.getProperty(MISSING_SCH_DATE_MESSAGE));            
        }
        if (time.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_TIME_TITLE), props.getProperty(MISSING_SCH_TIME_MESSAGE));            
        }
        if (topic.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_TOPIC_TITLE), props.getProperty(MISSING_SCH_TOPIC_MESSAGE));            
        }
        if (link.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_LINK_TITLE), props.getProperty(MISSING_SCH_LINK_MESSAGE));            
        }
        if (crit.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_SCH_CRIT_TITLE), props.getProperty(MISSING_SCH_CRIT_MESSAGE));            
        }
        
        else{
        
        sch.setTime(time);
        sch.setTitle(title);
        sch.setTopic(topic);
        sch.setLink(link);
        sch.setCriteria(crit);
        sch.setType(type);
        sch.setDate(date);
        tableData.set(selIndex, sch);
        }
        
        timeTextField.setText("");
        topicTextField.setText("");
        titleTextField.setText("");
        linkTextField.setText("");
        critTextField.setText("");
        typeCombo.getSelectionModel().clearSelection();
        //datep.getEditor().clear();
        datep.setFocusTraversable(true);

        }
        
        //workspace.reloadOfficeHoursGrid(data);
        //};
        
        catch(NullPointerException e){
            System.err.println("Cannot select when no TA in table, would cause exception: " + e);
        }
        
    }
}

