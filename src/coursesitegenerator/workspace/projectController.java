/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.studentItem;
import coursesitegenerator.data.teamsItem;
import javafx.collections.ObservableList;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class projectController {
    CourseSiteGenerator app;
    
    public projectController(CourseSiteGenerator initApp){
        app = initApp;
    }
    
    public void handleSelectTeam()
    {
        
             csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
             projectWorkspace projWork = workspace.getProjectsWorkspace();
        
             TableView teamTable = projWork.getTeamTable();
             //TableView studentTable = projWork.getStudentTable();
             Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
             
             teamsItem team = (teamsItem)selectedItem;
             if( !(selectedItem == null)) {
            
                    TextField nameTextField = projWork.getNameText();
                    TextField linkTextField = projWork.getLinkText();
                    ColorPicker color = projWork.getColor();
                    ColorPicker textColor = projWork.getTextColor();
                    
  
                    String name = team.getName();
                    String link = team.getLink();
                    String col = team.getColor();
                            //projWork.getColor().getValue().toString();
                    String textCol = team.getTextColor();
                            //projWork.getTextColor().getValue().toString();
                   
                    nameTextField.setText(name);
                    linkTextField.setText(link);
                    color.setValue(Color.web(col));
                    textColor.setValue(Color.web(textCol));
                    //color.setValue();
                   
             } 
             else
                 System.out.println("Nothing selected");
             
    }
    
    
    public void handleSelectStudent()
    {
        
             csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
             projectWorkspace projWork = workspace.getProjectsWorkspace();
        
             TableView studTable = projWork.getStudentTable();
             
             Object selectedItem = studTable.getSelectionModel().getSelectedItem();
             
             studentItem student = (studentItem)selectedItem;
             if( !(selectedItem == null)) {
            
                    TextField fnameTextField = projWork.getFNameText();
                    TextField lnameTextField = projWork.getLNameText();
                    ComboBox team = projWork.getTeamCombo();
                    TextField roleText = projWork.getRoleText();
  
                    String fname = student.getFName();
                    String lname = student.getLName();
                    String role = student.getRole();
                    String teamName = student.getTeamName();
                   
                    fnameTextField.setText(fname);
                    lnameTextField.setText(lname);
                    roleText.setText(role);
                    team.setValue(teamName);
      
             } 
             else
                 System.out.println("Nothing selected");
             
    }
        
    public void handleAddTeam(){
    csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
    projectWorkspace teamWork = workspace.getProjectsWorkspace();
    
    csgData data = (csgData)app.getDataComponent();
    ObservableList<teamsItem> teams = data.getTeams();
    
    TextField nameTextField = teamWork.getNameText();
    String name = nameTextField.getText();
    TextField linkTextField = teamWork.getLinkText();
    String link = linkTextField.getText();
    
    ColorPicker Color = teamWork.getColor();
    ColorPicker TextColor = teamWork.getTextColor();
    String color = Color.getValue().toString();
    String textColor = TextColor.getValue().toString();
    ComboBox teamBox = teamWork.getTeamCombo();
    teamBox.getItems().add(name);
    
    // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
  
    
    // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
    PropertiesManager props = PropertiesManager.getPropertiesManager();

    data.addTeam(name, color, textColor, link );

        
        // CLEAR THE TEXT FIELDS
        nameTextField.setText("");
        linkTextField.setText("");
       
    }
    
    public void handleAddStudent(){
    csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
    projectWorkspace studWork = workspace.getProjectsWorkspace();
    
    csgData data = (csgData)app.getDataComponent();
    
    TextField fnameTextField = studWork.getFNameText();
    String fname = fnameTextField.getText();
    TextField lnameTextField = studWork.getLNameText();
    String lname = lnameTextField.getText();
    TextField roleTextField = studWork.getRoleText();
    String role = roleTextField.getText();
    ComboBox teamBox = studWork.getTeamCombo();
    String team = (String) teamBox.getSelectionModel().getSelectedItem();
    // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
    
    // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
    PropertiesManager props = PropertiesManager.getPropertiesManager();

    data.addStudent(fname, lname, team, role);

        
        // CLEAR THE TEXT FIELDS
        fnameTextField.setText("");
        lnameTextField.setText("");
        roleTextField.setText("");
        
        
        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
       // nameTextField.requestFocus();
        
        // WE'VE CHANGED STUFF
        //markWorkAsEdited();
        //}
    }
    public void handleRemoveTeam()
    {
        try{
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        TableView teamTable = workspace.getProjectsWorkspace().getTeamTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        teamsItem team = (teamsItem)selectedItem;
        
        csgData data = (csgData)app.getDataComponent();
        ObservableList<teamsItem> tableData = data.getTeams();
        String teamName = team.getName();
        String link = team.getLink();
        String color = team.getColor();
        String textColor = team.getColor();
        tableData.remove(team);
        ComboBox teams = workspace.getScheduleWorkspace().getTypeCombo();
        TableView studentTable = workspace.getProjectsWorkspace().getStudentTable();
        ObservableList<studentItem> studs = data.getStudents();

         
        
       if(studentTable.getItems().contains(teamName)){
              studentTable.getItems().remove(teamName);
              //studentTable.getItems().remove
              //studs.get(0)
//            ta2.getItems().remove(taName);
//            recTable.getItems().remove(taName);
       }
       
        
        }
        catch(NullPointerException e)
        {
            System.err.println("Cannot delete when no TA in table, would cause exception: " + e);
        }
    }
    
    public void handleUpdateTeam(){
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        projectWorkspace projWork = workspace.getProjectsWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TableView teamTable = projWork.getTeamTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        csgData data = (csgData)app.getDataComponent();
        ObservableList<teamsItem> teamTableData = data.getTeams();
        int selIndex = teamTableData.indexOf(selectedItem);
        try{
        teamsItem team = (teamsItem)selectedItem;
        
        String teamname = team.getName();
        String color = team.getColor();
        String textColor = team.getTextColor();
        String link = team.getLink();
        TextField nameTextField = projWork.getNameText();
        TextField linkTextField = projWork.getLinkText();
        ColorPicker colorP = projWork.getColor();
        ColorPicker textColorP = projWork.getTextColor();
        
        String teamName = nameTextField.getText();
        String Link = linkTextField.getText();
        String Colorr = colorP.getValue().toString();
        String TextColor = textColorP.getValue().toString();
        

        
        //else{
        team.setName(teamName);
        team.setLink(Link);
        team.setColor(Colorr);
        team.setTextColor(TextColor);
        teamTableData.set(selIndex, team);
         
          nameTextField.setText("");
          linkTextField.setText("");
          colorP.setValue(Color.WHITE);
          textColorP.setValue(Color.WHITE);
        
        //workspace.reloadOfficeHoursGrid(data);
        }
        catch(NullPointerException e){
            System.err.println("Cannot select when no TA in table, would cause exception: " + e);
        }
    }

    
}
