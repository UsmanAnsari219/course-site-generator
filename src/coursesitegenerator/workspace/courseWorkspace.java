/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;
import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.CourseSiteGeneratorProp;
import coursesitegenerator.data.SitePages;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.csgData;
import coursesitegenerator.workspace.csgWorkspace;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
/**
 *
 * @author Usman
 */
public class courseWorkspace{
    csgData data;
    Tab CourseDetails;
    GridPane couP1;
    Pane couP2;
    VBox couV1;
    VBox couV2;
    VBox couV2b;
    HBox couH1;
    HBox couH2;
    HBox couH3;
    HBox couH4;
    VBox couV3;
    VBox couV3b;
    VBox couVOG;
    Button couChange1;
    Button couChange2;
    Button couChange3;
    
    Button sTemp;
    Button couChange4;
    ComboBox   subjCom;
    ComboBox    semCom;
    ComboBox    numCom;
    ComboBox   yearCom;
    TextField    titleText;
    TextField   insnamText;
    TextField   inshomeText;
    TableView   sitePages;
    ComboBox styleSh;
    CourseSiteGenerator app;
    
    public courseWorkspace(CourseSiteGenerator initApp) {
        app = initApp;
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
       
        CourseDetails = new Tab();
        
        couV1 = new VBox();
        couV2 = new VBox();
        couV2b = new VBox();
        couV3 = new VBox();
        couV3b = new VBox();
        couH1 = new HBox(35);
        couH2 = new HBox(53);
        couH3 = new HBox(45);
        couH4 = new HBox(125);
        couVOG = new VBox();
        styleSh = new ComboBox();
        couP1 = new GridPane();
       
        //Course Details
        Label courseInfo = new Label();
        Label siteTemp = new Label();
        Label siteTemptex = new Label();
        Label siteP = new Label();
        Label template = new Label();
        Label pageSty = new Label();
        Label subject = new Label();
        Label number = new Label();
        Label semester = new Label();
        Label year = new Label();
        Label title = new Label();
        Label insName = new Label();
        Label insHome = new Label();
        Label expDir = new Label();
        Label sitePa = new Label();
        Label UseC = new Label();
        Label NavTit = new Label();
        Label ScriptC = new Label();
        Label banSc = new Label();
        Label leftF = new Label();
        Label rightF = new Label();
        Label styleS = new Label();
        couChange1 = new Button();
        couChange2 = new Button();
        couChange3 = new Button();
        sTemp = new Button();
        couChange4 = new Button();
        subjCom = new ComboBox();
        semCom = new ComboBox();
        numCom = new ComboBox();
        yearCom = new ComboBox();
        titleText = new TextField();
        insnamText = new TextField();
        inshomeText = new TextField();
        sitePages = new TableView();
        titleText.setPromptText("Computer Science III");
        insnamText.setPromptText("Richard McKenna");
        inshomeText.setPromptText("http://www.cs.stonybrook.edu/~richard");
        
        
        
        courseInfo.setText(props.getProperty(CourseSiteGeneratorProp.COURSEINFO.toString()));
        siteTemp.setText(props.getProperty(CourseSiteGeneratorProp.SITETEMP.toString()));
        siteTemptex.setText(props.getProperty(CourseSiteGeneratorProp.SITETEMP_PROMPT.toString()));
        pageSty.setText(props.getProperty(CourseSiteGeneratorProp.PAGESTY.toString()));
        subject.setText(props.getProperty(CourseSiteGeneratorProp.SUBJECT.toString()));
        number.setText(props.getProperty(CourseSiteGeneratorProp.NUMBER.toString()));
        semester.setText(props.getProperty(CourseSiteGeneratorProp.SEMESTER.toString()));
        year.setText(props.getProperty(CourseSiteGeneratorProp.YEAR.toString()));
        title.setText(props.getProperty(CourseSiteGeneratorProp.TITLE.toString()));
        insName.setText(props.getProperty(CourseSiteGeneratorProp.INSNAME.toString()));
        insHome.setText(props.getProperty(CourseSiteGeneratorProp.INSHOME.toString()));
        expDir.setText(props.getProperty(CourseSiteGeneratorProp.EXPDIR.toString()));
        sitePa.setText(props.getProperty(CourseSiteGeneratorProp.SITEPA.toString()));
        UseC.setText(props.getProperty(CourseSiteGeneratorProp.USEC.toString()));
        NavTit.setText(props.getProperty(CourseSiteGeneratorProp.NAVTIT.toString()));
        ScriptC.setText(props.getProperty(CourseSiteGeneratorProp.SCRIPTC.toString()));
        banSc.setText(props.getProperty(CourseSiteGeneratorProp.BANSC.toString()));
        leftF.setText(props.getProperty(CourseSiteGeneratorProp.LEFTF.toString()));
        rightF.setText(props.getProperty(CourseSiteGeneratorProp.RIGHTF.toString()));
        styleS.setText(props.getProperty(CourseSiteGeneratorProp.STYLES.toString()));
        couChange1.setText(props.getProperty(CourseSiteGeneratorProp.COUCHANGE.toString()));
        couChange2.setText(props.getProperty(CourseSiteGeneratorProp.COUCHANGE.toString()));
        couChange3.setText(props.getProperty(CourseSiteGeneratorProp.COUCHANGE.toString()));
        couChange4.setText(props.getProperty(CourseSiteGeneratorProp.COUCHANGE.toString()));
        sTemp.setText("Select Template Directory");
        TableColumn use = new TableColumn(props.getProperty(CourseSiteGeneratorProp.USEC.toString()));
        TableColumn nav = new TableColumn(props.getProperty(CourseSiteGeneratorProp.NAVTIT.toString()));
        TableColumn fileN = new TableColumn(props.getProperty(CourseSiteGeneratorProp.FILEN.toString()));
        TableColumn script = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCRIPTC.toString()));
        
        data = (csgData) app.getDataComponent();
        ObservableList<SitePages> siteData = data.getSitePagesList();
        sitePages.setItems(siteData);
        
        use.setCellValueFactory(
                new PropertyValueFactory<SitePages, Boolean>("use")
        );
        nav.setCellValueFactory(
                new PropertyValueFactory<SitePages, String>("title")
        );
        fileN.setCellValueFactory(
                new PropertyValueFactory<SitePages, String>("fileName")
        );
        script.setCellValueFactory(
                new PropertyValueFactory<SitePages, String>("script")
        );
        use.setCellFactory(column -> new CheckBoxTableCell());
        
        
        
        sitePages.getColumns().add(use);
        sitePages.getColumns().add(nav);
        sitePages.getColumns().add(fileN);
        sitePages.getColumns().add(script);
        sitePages.setEditable(true);
        
        
        subjCom.getItems().addAll("CSE","IST");
        semCom.getItems().addAll("Fall","Spring","Summer");
        numCom.getItems().addAll("219","308","380","102");
        yearCom.getItems().addAll("2017","2018","2019","2020");
        
       
        
        //couP1.getColumnConstraints().add(new ColumnConstraints(200));
        
        subjCom.setMinWidth(120);
        couP1.add(courseInfo, 0, 0);
        courseInfo.getStyleClass().add("titles");
        couP1.add(subject, 0, 1);
        couP1.add(subjCom, 1, 1);
        couP1.add(number, 2, 1);
        number.setPadding(new Insets(0,30,0,150));
        couP1.add(numCom, 3, 1);
        numCom.setMinWidth(120);
        couP1.add(semester, 0, 2);
        couP1.add(semCom, 1, 2);
        semCom.setMinWidth(120);
        couP1.add(year, 2, 2);
        year.setPadding(new Insets(0,30,0,150));
        couP1.add(yearCom, 3, 2);
        yearCom.setMinWidth(120);
        couP1.add(title, 0, 3);
        couP1.add(titleText, 1, 3);
        GridPane.setColumnSpan(titleText, 3);
        couP1.add(insName, 0, 4);
        couP1.add(insnamText, 1, 4);
        GridPane.setColumnSpan(insnamText, 3);
        couP1.add(insHome, 0 ,5);
        couP1.add(inshomeText, 1, 5);
        GridPane.setColumnSpan(inshomeText, 3);
        couP1.add(expDir, 0, 6);
        couP1.add(couChange4, 1, 6);
        
        
        couV1.getChildren().add(couP1);
        couV2.getChildren().add(siteTemp);
        siteTemp.getStyleClass().add("titles");
        couV2.getChildren().add(siteTemptex);  
        couV2.getChildren().add(sTemp);
        couV2.getChildren().add(template);
        couV2.getChildren().add(siteP);
        couV2.getChildren().add(sitePages);
        couV2b.getChildren().add(couV2);
        couV3.getChildren().add(pageSty);
        pageSty.getStyleClass().add("titles");
        couH1.getChildren().add(banSc);
        couH1.getChildren().add(couChange1);
        //couChange1.setPadding(new Insets(3,10,3,10));
        //couH1.setPadding(new Insets(5,5,5,5));
        couH2.getChildren().addAll(leftF,couChange2);
        //couV3.setPadding(new Insets(100, 0, 0, 0));
        //couChange2.setPadding(new Insets(3,10,3,10));
        //couChange2.set
        //couH2.setSpacing(10);
        //couH2.setPadding(new Insets(5,5,5,5));
        couH3.getChildren().add(rightF);
        couH3.getChildren().add(couChange3);
       // couChange3.setPadding(new Insets(3,10,3,10));
        //couH3.setPadding(new Insets(5,5,5,5));
        couH4.getChildren().add(styleS);
        couH4.getChildren().add(styleSh);
        //couH4.setPadding(new Insets(5,5,5,5));
        couV3.getChildren().add(couH1);
        couV3.getChildren().add(couH2);
        couV3.getChildren().add(couH3);
        couV3.getChildren().add(couH4);
        couV3b.getChildren().add(couV3);
        couVOG.getChildren().add(couV1);
        couVOG.getChildren().add(couV2b);
        couVOG.getChildren().add(couV3b);
        
        CourseDetails.setContent(couVOG);
        CourseDetails.setClosable(false);
        
         
    }
    
     public GridPane getCouP1(){
        return couP1;
    }
    public VBox getCouV1(){
        return couV1;
    }
    
    public VBox getCouVOG(){
        return couVOG;
    }
    
    public VBox getCouV2(){
        return couV2;
    }    
    
    public TableView getSitePages(){
        return sitePages;
    }
    
     public VBox getCouV2b() {
        return couV2b;
    }
    
    public VBox getCouV3b(){
        return couV3b;
    }
    
    public VBox getCouV3(){
        return couV3;
    }
    public Tab getCourseDetails(){
        return CourseDetails;
    }
    
    
}
