/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.CourseSiteGeneratorProp;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.scheduleItem;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class scheduleWorkspace {
    CourseSiteGenerator app;
    scheduleController controller;
    csgData data;
    Tab ScheduleTab;
    HBox addHBox;
    VBox sched;
    VBox schedb;
    VBox schedbb;
    VBox calBound;
    HBox calDates;
    VBox calDatesb;
    VBox schedItems;
    VBox schedItemsb;
    TableView schedItems1;
    GridPane schedAdd;
    DatePicker monText;
    DatePicker friText;
    ComboBox typeCombo;
    DatePicker datePick;
    TextField timeText;
    TextField schTitleText;
    TextField topicText;
    TextField linkText;
    TextField critText;
    Button addUpSc;
    Button updateSc;
    Button clearSc;
    HBox buttonBoxSc;
    
    public scheduleWorkspace(CourseSiteGenerator initApp) {
        app = initApp;
        controller = new scheduleController(app);
        ScheduleTab = new Tab();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        addHBox = new HBox();
        sched = new VBox();
        schedb = new VBox();
        schedbb = new VBox();
        calBound = new VBox();
        calDates = new HBox();
        calDatesb = new VBox();
        schedItems = new VBox();
        schedItemsb = new VBox();
        schedItems1 = new TableView();
        schedAdd = new GridPane();
        monText = new DatePicker();
        friText = new DatePicker();
        typeCombo = new ComboBox();
        datePick = new DatePicker();
        timeText = new TextField();
        schTitleText = new TextField();
        topicText = new TextField();
        linkText = new TextField();
        critText = new TextField();
        addUpSc = new Button();
        clearSc = new Button();
        updateSc = new Button();
        buttonBoxSc = new HBox();
        typeCombo = new ComboBox();
        datePick = new DatePicker();
        timeText = new TextField();
        schTitleText = new TextField();
        topicText = new TextField();
        linkText = new TextField();
        critText = new TextField();
        
        addUpSc.setText(props.getProperty(CourseSiteGeneratorProp.ADD_BUTTON_TEXT));
        updateSc.setText(props.getProperty(CourseSiteGeneratorProp.UPDATE_BUTTON_TEXT));
        clearSc.setText((props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT)));
        
        Label schedule = new Label();
        schedule.setText(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_LABEL.toString()));
        Label calenderBound = new Label();
        calenderBound.setText(props.getProperty(CourseSiteGeneratorProp.CALBOUND.toString()));
        Label startMon = new Label();
        startMon.setText(props.getProperty(CourseSiteGeneratorProp.STARTMON.toString()));
        Label endFri = new Label();
        endFri.setText(props.getProperty(CourseSiteGeneratorProp.ENDFRI.toString()));
        Label schedItemsL = new Label();
        schedItemsL.setText(props.getProperty(CourseSiteGeneratorProp.SCHEDITEMSL.toString()));
        Label typeLabel = new Label();
        typeLabel.setText(props.getProperty(CourseSiteGeneratorProp.TYPELABEL.toString()));
        Label dateLabel = new Label();
        dateLabel.setText(props.getProperty(CourseSiteGeneratorProp.DATELABEL.toString()));
        Label timeLabel = new Label();
        timeLabel.setText(props.getProperty(CourseSiteGeneratorProp.TIMELABEL.toString()));
        Label titleLabel = new Label();
        Label topicLabel = new Label();
        topicLabel.setText(props.getProperty(CourseSiteGeneratorProp.TOPICLABEL.toString()));
        titleLabel.setText(props.getProperty(CourseSiteGeneratorProp.TITLELABEL.toString()));
        Label linkLabel = new Label();
        linkLabel.setText(props.getProperty(CourseSiteGeneratorProp.LINKLABEL.toString()));
        Label criteriaLabel = new Label();
        criteriaLabel.setText(props.getProperty(CourseSiteGeneratorProp.CRITERIALABEL.toString()));
        Label addE = new Label();
        addE.setText(props.getProperty(CourseSiteGeneratorProp.AE_TEXT.toString()));
        
        TableColumn schTypeC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TYPECOL.toString()));
        TableColumn schDateC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.DATECOL.toString()));
        TableColumn schTitleC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TITLECOL.toString()));
        TableColumn schTopicC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TOPICCOL.toString()));
        
        buttonBoxSc.getChildren().addAll(addUpSc,clearSc);
        
        data = (csgData) app.getDataComponent();
        ObservableList<scheduleItem> schedData = data.getSchedules();
        schedItems1.setItems(schedData);
        
        schTypeC.setCellValueFactory(
                new PropertyValueFactory<scheduleItem, String>("type")
        );
        schDateC.setCellValueFactory(
                new PropertyValueFactory<scheduleItem, String>("date")
        );
        schTitleC.setCellValueFactory(
                new PropertyValueFactory<scheduleItem, String>("title")
        );
        schTopicC.setCellValueFactory(
                new PropertyValueFactory<scheduleItem, String>("topic")
        );
        
        typeCombo.getItems().addAll("Holiday","Lecture","HW","Recitation","Reference");
        
        
        
        schedItems1.getColumns().addAll(schTypeC,schDateC,schTitleC,schTopicC);
        calDates.getChildren().addAll(startMon,monText,endFri,friText);
        calDates.setSpacing(30);
        calDatesb.getChildren().add(calDates);
        
        schedAdd.add(typeLabel, 0, 0);
        schedAdd.add(dateLabel, 0, 1);
        schedAdd.add(timeLabel, 0, 2);
        schedAdd.add(titleLabel, 0, 3);
        schedAdd.add(topicLabel, 0, 4);
        schedAdd.add(linkLabel, 0 , 5);
        schedAdd.add(criteriaLabel, 0, 6);
        schedAdd.add(typeCombo, 1, 0);
        schedAdd.add(datePick, 1, 1);
        schedAdd.add(timeText, 1, 2);
        schedAdd.add(schTitleText, 1, 3);
        schTitleText.getStyleClass().add("titles");
        schedAdd.add(topicText, 1, 4);
        schedAdd.add(linkText, 1, 5);
        schedAdd.add(critText, 1, 6);
        
        //addE.setFont(Font.font("Verdana" ,FontWeight.BOLD, 16));
        //schedItems.getChildren().addAll(schedItemsL,schedItems1, addHBox, schedAdd);
        schedItems.getChildren().addAll(schedItemsL,schedItems1,addE,schedAdd,buttonBoxSc);
        schedItemsL.getStyleClass().add("titles");
        buttonBoxSc.setPadding(new Insets(10, 10, 10, 0));
        buttonBoxSc.setSpacing(30);
        addE.setPadding(new Insets(10, 0, 10, 0));
        
        schedItemsb.getChildren().addAll(schedItems);
        sched.getChildren().addAll(schedule,calDatesb,schedItemsb);
        schedb.getChildren().add(sched);
        schedbb.getChildren().add(schedb);
        sched.setSpacing(10);
        
        ScheduleTab.setContent(schedbb);
        ScheduleTab.setClosable(false);
        
        schedItems1.setOnMouseReleased(e ->{
            controller.handleSelectSch();
            updateSet(); 
        });
        addUpSc.setOnAction((ActionEvent e) -> {
            controller.handleAddSch();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
        });
        schedItems1.setOnKeyPressed(e->{
            if(e.getCode()== KeyCode.BACK_SPACE || e.getCode()== KeyCode.DELETE) {
                controller.handleRemoveSch();
                app.getGUI().getFileController().markAsEdited(app.getGUI());
        }});
        clearSc.setOnAction((ActionEvent e) -> {
            controller.handleClearSch();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            addSet();
        });
        updateSc.setOnAction((ActionEvent e) -> {
            controller.handleUpdateSch();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            addSet();
        });
        
        
    }
    
    public VBox getSchedB(){
        return schedb;
    }
    public VBox getSchedBB(){
        return schedbb;
    }
    public VBox getSchedItemsb(){
        return schedItemsb;
    }
    public TableView getSchedItemsTable(){
        return schedItems1;
    }
    public VBox getCalDatesb(){
        return calDatesb;
    }
    public TextField getTimeTextField(){
        return timeText;
    }
    public ComboBox getTypeCombo(){
        return typeCombo;
    }
    public DatePicker getDatePick(){
        return datePick;
    }
    public TextField getTitleTextField(){
        return schTitleText;
    }
    public TextField getTopicTextField(){
        return topicText;
    }
    public TextField getLinkTextField(){
        return linkText;
    }
    public TextField getCriteriaTextField(){
        return critText;
    }
    public Tab getScheduleTab() {
        return ScheduleTab;
    }
    public void updateSet() {
        if(buttonBoxSc.getChildren().contains(updateSc))
                return;
         buttonBoxSc.getChildren().remove(addUpSc);
         buttonBoxSc.getChildren().add(0, updateSc);
         
    }
    
    public void addSet()  {
        if(buttonBoxSc.getChildren().contains(addUpSc))
            return;
        buttonBoxSc.getChildren().remove(updateSc);
       
        buttonBoxSc.getChildren().add(0, addUpSc);
    }
}
