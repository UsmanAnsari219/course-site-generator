/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import static coursesitegenerator.CourseSiteGeneratorProp.INCORRECT_TA_EFORMAT_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.INCORRECT_TA_EFORMAT_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.INCORRECT_TREQUEST_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.INCORRECT_TREQUEST_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_TA_NAME_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_TA_NAME_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_TA_TIME_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.MISSING_TA_TIME_TITLE;
import static coursesitegenerator.CourseSiteGeneratorProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static coursesitegenerator.CourseSiteGeneratorProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.recitationItem;
import static coursesitegenerator.style.csgStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static coursesitegenerator.style.csgStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static coursesitegenerator.style.csgStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import djf.ui.AppMessageDialogSingleton;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import jtps.jTPS;
import jtps.jTPS_Transaction;

/**
 *
 * @author Usman
 */
public class csgController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    CourseSiteGenerator app;
    //private jTPS jtps;
    jTPS jTPS = new jTPS();

    /**
     * Constructor, note that the app must already be constructed.
     */
    public csgController(CourseSiteGenerator initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
       // jTPS jtps = new jTPS();
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        recitationWorkspace recWork = workspace.getRecitationWorkspace();
        TextField nameTextField = workspace.getTADataWorkspace().getNameTextField();
        String name = nameTextField.getText();
        TextField emailTextField = workspace.getTADataWorkspace().getEmailTextField();
        String email = emailTextField.getText();
        ComboBox ta1Box = recWork.getTA1Box();
        ComboBox ta2Box = recWork.getTA2Box();
        
        TableView taTable = workspace.getTADataWorkspace().getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = null;
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        csgData data = (csgData)app.getDataComponent();
        ObservableList teachingAssistants = data.getTeachingAssistants();
         ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Boolean und = false;
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name,email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else { 
            if (validEmail(email))
            {
                
                data.addTA(name,email,und);
                ta = data.getTA(name);
                String taNam = ta.getName();
                ta1Box.getItems().add(taNam);
                ta2Box.getItems().add(taNam);
                jTPS_Transaction transaction = new addTATrans(ta ,app, data,tableData);
                jTPS.addTransaction(transaction);
                System.out.println("addhandler");
                //data.addTA(name,email);
                //jTPS_transaction = new addTATrans(transaction);
            }
            //else if(matcher.matches() == true && )
            else{
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INCORRECT_TA_EFORMAT_TITLE), props.getProperty(INCORRECT_TA_EFORMAT_MESSAGE));
                }
            
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
           // nameTextField.requestFocus();
            
            // WE'VE CHANGED STUFF
            //markWorkAsEdited();
        }
    }
    
    public boolean validEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
            matcher = pattern.matcher(email);
        return matcher.matches() == true;
    }
    
    public void handleDeleteTA() {
        
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        
        TableView taTable = workspace.getTADataWorkspace().getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        Boolean und = ta.getUnd();
        csgData data = (csgData)app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        
        data.deleteTA(taName, "",und);
    }
    
    public void handleRemoveTAData()
    {
        try{
         csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
       
        TableView taTable = workspace.getTADataWorkspace().getTATable();
        TableView recTable = workspace.getRecitationWorkspace().getRecTable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        
        csgData data = (csgData)app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        ObservableList<recitationItem> recTableData = data.getRecitations();
        String taName = ta.getName();
        tableData.remove(ta);
        ComboBox ta1 = workspace.getRecitationWorkspace().getTA1Box();
        ComboBox ta2 = workspace.getRecitationWorkspace().getTA2Box();
        
        if(ta1.getItems().contains(taName) || ta2.getItems().contains(taName)){
            ta1.getItems().remove(taName);
            ta2.getItems().remove(taName);
            recTable.getItems().remove(taName);
        }
       
        HashMap<String, StringProperty> officeHours = data.getOfficeHours();
        //jTPS_Transaction transaction1 = new deleteTATrans(ta, data, officeHours);
        //jTPS.addTransaction(transaction1);

        for(String key: officeHours.keySet()){
        
           if(officeHours.get(key).getValue().contains(taName)){
              
               data.removeTAFromCell(officeHours.get(key), taName);
           }
        }
        }
        catch(NullPointerException e)
        {
            System.err.println("Cannot delete when no TA in table, would cause exception: " + e);
        }
    }
    
    public void handleSelectTA()
    {
        
             csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        
             TableView taTable = workspace.getTADataWorkspace().getTATable();
             TextField nameTextField = workspace.getTADataWorkspace().getNameTextField();
             TextField emailTextField = workspace.getTADataWorkspace().getEmailTextField();
             Object selectedItem = taTable.getSelectionModel().getSelectedItem();
             
            
             if( !(selectedItem == null)) {
                    
                    TeachingAssistant ta = (TeachingAssistant)selectedItem;
                   
  
                    String taName = ta.getName();
                    String taEmail = ta.getEmail();
                    
                    nameTextField.setText(taName);
                    emailTextField.setText(taEmail);
             } 
             else
                 System.out.println("Nothing selected");
    }
    
   public void handleUpdateTA(){
       csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
       tadataWorkspace taWork = workspace.getTADataWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
       TableView taTable = taWork.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        csgData data = (csgData)app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        int selIndex = tableData.indexOf(selectedItem);
        try{
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        
        String taName = ta.getName();
        String taEmail = ta.getEmail();
        TextField nameTextField = taWork.getNameTextField();
        //String name = nameTextField.getText();
        TextField emailTextField = taWork.getEmailTextField();
        //ta.setName(nameTextField.getText());
        //ta.setEmail(emailTextField.getText());
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        HashMap<String, StringProperty> officeHours = data.getOfficeHours();
        
       
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (name.equals(taName) && email.contains(taEmail)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        else if(!validEmail(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INCORRECT_TA_EFORMAT_TITLE), props.getProperty(INCORRECT_TA_EFORMAT_MESSAGE));
        }
        
        else{
        ta.setName(name);
        ta.setEmail(email);
        tableData.set(selIndex, ta);
         for(String key: officeHours.keySet()){
            
           if(officeHours.get(key).getValue().contains(taName)){
               data.updateTAInCell(officeHours.get(key), taName, name);
           }
        }
          nameTextField.setText("");
          emailTextField.setText("");
          nameTextField.requestFocus();
        
        //workspace.reloadOfficeHoursGrid(data);
        }
        }
        catch(NullPointerException e){
            System.err.println("Cannot select when no TA in table, would cause exception: " + e);
        }
        
        
    }
   
   public void handleTimeChange(String startTime, String endTime){
       int mStart = militaryTimeConverter(startTime);
       int mEnd = militaryTimeConverter(endTime);
       csgData data = (csgData)app.getDataComponent();
       
       if(mStart > mEnd )
       {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(INCORRECT_TREQUEST_TITLE), props.getProperty(INCORRECT_TREQUEST_MESSAGE));
       }
       else if(mStart == mEnd)
       {
           PropertiesManager props = PropertiesManager.getPropertiesManager();
           AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
           dialog.show(props.getProperty(INCORRECT_TREQUEST_TITLE), props.getProperty(INCORRECT_TREQUEST_MESSAGE));
           
       }
       else if(mStart == 12) {
           data.editTime( mStart, mEnd);
       }
       else{
           data.editTime(mStart,mEnd);
       } 
   }
   
   public void handleClearTA(){
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        tadataWorkspace taWork = workspace.getTADataWorkspace();
        TableView taTable = taWork.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        TextField nameTextField = taWork.getNameTextField();
        //String name = nameTextField.getText();
        TextField emailTextField = taWork.getEmailTextField();
        try{
       String name = nameTextField.getText();
       String email = emailTextField.getText();
////        //ta.setName(name);
//        //ta.setEmail(email);
        emailTextField.setText("");
        nameTextField.setText("");
        jTPS_Transaction transaction = (jTPS_Transaction) new clearTrans(nameTextField, emailTextField, name, email);
        jTPS.addTransaction(transaction);
        
        }catch(NullPointerException noe) {}
        nameTextField.requestFocus();
        
        
   }
   
   public int militaryTimeConverter(String time){
       int mTime = 0;
       if(time == null) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_TIME_TITLE), props.getProperty(MISSING_TA_TIME_MESSAGE));
       }
       
       else{
           int index = time.indexOf(":");
           mTime = Integer.parseInt(time.substring(0,index));
           if(time.contains("p") && mTime != 12)
               mTime += 12;
        }
       return mTime;
    }

   public void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        csgData data = (csgData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTADataWorkspace().getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getTADataWorkspace().getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getTADataWorkspace().getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getTADataWorkspace().getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTADataWorkspace().getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTADataWorkspace().getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    public void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        csgData data = (csgData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        
        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTADataWorkspace().getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);
        
        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getTADataWorkspace().getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getTADataWorkspace().getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getTADataWorkspace().getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTADataWorkspace().getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTADataWorkspace().getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }
    public void handleUndo(){
        //TAData data = (TAData)app.getDataComponent();
        //jTPS = data.getJTPS();
        jTPS.undoTransaction();
        System.out.println("handleundo");
    }
    
    public void handleRedo() {
        
        jTPS.doTransaction();
    }
    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        csgWorkspace workspace = (csgWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTADataWorkspace().getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if( selectedItem == null)
        {
            return;
        }
        // GET THE TA
        else{
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String taName = ta.getName();
        String taEmail = ta.getEmail();
        csgData data = (csgData)app.getDataComponent();
        String cellKey = pane.getId(); 
        

        if(data.containsTA(taName,taEmail)) {
           handleDeleteTA();
            data.toggleTAOfficeHours(cellKey,taName); }
        else
           handleDeleteTA();
        }
       
 } 
    
}
