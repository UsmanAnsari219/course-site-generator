/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.CourseSiteGeneratorProp;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.studentItem;
import coursesitegenerator.data.teamsItem;
import coursesitegenerator.data.teamsItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import properties_manager.PropertiesManager;

/**
 *
 * @author Usman
 */
public class projectWorkspace {
    CourseSiteGenerator app;
    projectController controller;
    csgData data;
    Tab ProjectTab;
    VBox projs;
    VBox projs1;
    VBox projs2;
    TableView teams;
    GridPane teamsAE;
    TableView studs;
    GridPane students;
    TextField namePText;
    TextField linkPText;
    TextField fnamePText;
    TextField lnamePText;
    ComboBox teamPCombo;
    TextField rolePText;
    Label projects;
    ColorPicker color;
    ColorPicker textColor;
    Button auP1;
    Button cP1;
    Button auP2;
    Button cP2;
    Button up1;
    Button up2;
    
    Circle c;
    ObservableList<teamsItem> teamsList;
    
    public projectWorkspace(CourseSiteGenerator initApp){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        app = initApp;
        controller = new projectController(app);
        
        ProjectTab = new Tab();
        projs = new VBox();
        projs1 = new VBox();
        projs2 = new VBox();
        teams = new TableView();
        teamsAE = new GridPane();
        studs = new TableView();
        students = new GridPane();
        namePText = new TextField();
        linkPText = new TextField();
        fnamePText = new TextField();
        lnamePText = new TextField();
        teamPCombo = new ComboBox();
        rolePText = new TextField();
        projects = new Label();
        projects.getStyleClass().add("titles");
        color = new ColorPicker();
        textColor = new ColorPicker();
        auP1 = new Button();
        cP1 = new Button();
        up1 = new Button();
        up2 = new Button();
        auP2 = new Button();
        cP2 = new Button();
        c = new Circle();
        teamsList = FXCollections.observableArrayList();

        
        projects.setText(props.getProperty(CourseSiteGeneratorProp.PROJECTS.toString()));
        Label teamsL = new Label();
        teamsL.setText(props.getProperty(CourseSiteGeneratorProp.TEAMS.toString()));
        teamsL.getStyleClass().add("titles");
        Label nameL = new Label();
        nameL.setText(props.getProperty(CourseSiteGeneratorProp.NAMEL.toString()));
        Label colorL = new Label();
        colorL.setText(props.getProperty(CourseSiteGeneratorProp.COLORL.toString()));
        Label textColorL = new Label();
        textColorL.setText(props.getProperty(CourseSiteGeneratorProp.TEXTCOLORL.toString()));
        Label linkL = new Label();
        linkL.setText(props.getProperty(CourseSiteGeneratorProp.LINKLABEL.toString()));
        Label studentsL = new Label();
        studentsL.getStyleClass().add("titles");
        studentsL.setText(props.getProperty(CourseSiteGeneratorProp.STUDENTSL.toString()));
        Label fNameL = new Label();
        fNameL.setText(props.getProperty(CourseSiteGeneratorProp.FNAMEL.toString()));
        Label lNameL = new Label();
        lNameL.setText(props.getProperty(CourseSiteGeneratorProp.LNAMEL.toString()));
        Label teamL = new Label();
        teamL.setText(props.getProperty(CourseSiteGeneratorProp.TEAML.toString()));
        Label roleL = new Label();
        roleL.setText(props.getProperty(CourseSiteGeneratorProp.ROLEL.toString()));
        Label ae = new Label();
        ae.setText(props.getProperty(CourseSiteGeneratorProp.AE_TEXT.toString()));
        
       
        TableColumn namePC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.NAMEPC.toString()));
        TableColumn colorPC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COLORPC.toString()));
        TableColumn textColorPC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEXTCOLORPC.toString()));
        TableColumn linkPC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.LINKPC.toString()));
        TableColumn fname = new TableColumn(props.getProperty(CourseSiteGeneratorProp.FNAMEC.toString()));
        TableColumn lname = new TableColumn(props.getProperty(CourseSiteGeneratorProp.LNAMEC.toString()));
        TableColumn team = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAMC.toString()));
        TableColumn roleC = new TableColumn(props.getProperty(CourseSiteGeneratorProp.ROLEC.toString()));
        
        auP1.setText(props.getProperty(CourseSiteGeneratorProp.ADDUP_TEXT));
        auP1.setPadding(new Insets(3,10,3,10));
        auP2.setText(props.getProperty(CourseSiteGeneratorProp.ADDUP_TEXT));
        cP1.setText(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT));
        cP1.setPadding(new Insets(3,10,3,10));
        cP2.setText(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT));
        up1.setText(props.getProperty(CourseSiteGeneratorProp.UPDATE_BUTTON_TEXT));
        up1.setPadding(new Insets(3,10,3,10));
        up2.setText(props.getProperty(CourseSiteGeneratorProp.UPDATE_BUTTON_TEXT));

        
        c.setRadius(1);
        c.setStrokeWidth(1);
        c.setStroke(Paint.valueOf(color.getValue().toString()));
       
        data = (csgData) app.getDataComponent();
        ObservableList<teamsItem> teamData = data.getTeams();
        teams.setItems(teamData);
        
        namePC.setCellValueFactory(
                new PropertyValueFactory<teamsItem, String>("name")
        );
        colorPC.setCellValueFactory(
                new PropertyValueFactory<teamsItem, String>("color")
        );
        textColorPC.setCellValueFactory(
                new PropertyValueFactory<teamsItem, String>("textColor")
        );
        linkPC.setCellValueFactory(
                new PropertyValueFactory<teamsItem, String>("link")
        );
        teams.getColumns().addAll(namePC,colorPC,textColorPC,linkPC);
        
       
        
        
        ObservableList<studentItem> studentData = data.getStudents();
        
        studs.setItems(studentData);
        
        fname.setCellValueFactory(
                new PropertyValueFactory<studentItem, String>("fName")
        );
        lname.setCellValueFactory(
                new PropertyValueFactory<studentItem, String>("lName")
        );
        team.setCellValueFactory(
                new PropertyValueFactory<studentItem, String>("teamName")
        );
        roleC.setCellValueFactory(
                new PropertyValueFactory<teamsItem, String>("role")
        );
        
        studs.getColumns().addAll(fname, lname, team, roleC);
        
        teamsAE.add(nameL, 0, 0);
        teamsAE.add(namePText, 1, 0);
        teamsAE.add(colorL, 0, 1);
        teamsAE.add(color, 1, 1);
        color.setPadding(new Insets(10,0,10,0));
        
        teamsAE.add(textColorL, 2, 1);
        teamsAE.add(textColor, 3, 1);
        textColor.setPadding(new Insets(10,0,10,0));
        teamsAE.add(linkL, 0, 2);
        teamsAE.add(linkPText, 1, 2);
        teamsAE.add(auP1, 0, 3);
        teamsAE.add(cP1, 1, 3);
        teamsAE.add(c, 2, 3);
        teamsAE.setHgap(10);
        teamsAE.setVgap(20);
        
        students.add(fNameL, 0, 0);
        students.add(fnamePText, 1, 0);
        students.add(lNameL, 0, 1);
        students.add(lnamePText, 1, 1);
        students.add(teamL, 0, 2);
        students.add(teamPCombo, 1, 2);
        students.add(roleL, 0, 3);
        students.add(rolePText, 1, 3);
        students.add(auP2, 0, 4);
        students.setHgap(10);
        students.setVgap(20);
        auP2.setPadding(new Insets(3,10,3,10));
        students.add(cP2, 1, 4);
        cP2.setPadding(new Insets(3,10,3,10));
        
        projs1.getChildren().addAll(teamsL,teams,ae,teamsAE);
        
        projs2.getChildren().addAll(studentsL,studs,ae,students);
        
        projs.getChildren().add(projects);
        projs.getChildren().add(projs1);
        projs.getChildren().add(projs2);
        projs.setSpacing(10);
        
         ProjectTab.setContent(projs);
         ProjectTab.setClosable(false);
         
         teams.setOnMouseReleased(e ->{
            controller.handleSelectTeam();
            updateSet1();
        });
         studs.setOnMouseReleased(e ->{
            controller.handleSelectStudent();
        });
        auP1.setOnAction((ActionEvent e) -> {
            controller.handleAddTeam();
            
            app.getGUI().getFileController().markAsEdited(app.getGUI());
        });
        auP2.setOnAction((ActionEvent e) -> {
            controller.handleAddStudent();
            
            app.getGUI().getFileController().markAsEdited(app.getGUI());
        });
        up1.setOnAction((ActionEvent e) -> {
            controller.handleUpdateTeam();
            app.getGUI().getFileController().markAsEdited(app.getGUI());
            //addButton.setText(addButtonText);
            addSet1();
        });
        teams.setOnKeyPressed((KeyEvent e) -> {
            if(e.getCode()== KeyCode.BACK_SPACE || e.getCode()== KeyCode.DELETE) {
                controller.handleRemoveTeam();
                app.getGUI().getFileController().markAsEdited(app.getGUI());
                addSet1();
            }
        });
    }
    
    public Tab getProjectTab(){
        return ProjectTab;
    }
    public void updateSet1() {
        if(teamsAE.getChildren().contains(up1))
                return;
         teamsAE.getChildren().remove(auP1);
         teamsAE.add(up1, 0 , 3);
         
    }
    
    public void addSet1()  {
        if(teamsAE.getChildren().contains(auP1))
            return;
        teamsAE.getChildren().remove(up1);
       
        teamsAE.add(auP1, 0, 3);

    }
    
     public VBox getProjs() {
        return projs;
    }
    public VBox getProjs1(){
        return projs1;
    }
   
    public VBox getProjs2() {
        return projs2;
    }
    public TableView getStudentTable(){
        return studs;
    }
    public TableView getTeamTable(){
        return teams;
    }
    public TextField getNameText(){
        return namePText;
    }
    public TextField getLinkText(){
        return linkPText;
    }
    public TextField getFNameText(){
        return fnamePText;
    }
    public TextField getLNameText(){
        return lnamePText;
    }
    public ColorPicker getColor(){
        return color;
    }
    public ComboBox getTeamCombo(){
        return teamPCombo;
    }
    public TextField getRoleText(){
        return rolePText;
    }
    public ColorPicker getTextColor(){
        return textColor;
    }
}
