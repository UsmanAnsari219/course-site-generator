/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.test_bed;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.data.SitePages;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.courseInfo;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.recitationItem;
import coursesitegenerator.data.scheduleItem;
import coursesitegenerator.data.studentItem;
import coursesitegenerator.data.teamsItem;
import coursesitegenerator.file.TimeSlot;
import coursesitegenerator.file.csgFiles;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.InvalidXMLFileFormatException;

/**
 *
 * @author Usman
 */
public class TestSave {
    /*
    CourseSiteGenerator app;
    TeachingAssistant ta;
    recitationItem rec;
    HashMap<String, StringProperty> officeHours;
    csgData dataComp;
    int startH;
    int endH; 
    ArrayList<TimeSlot> timeSlotData; */
    
    
    
    public TestSave() throws InvalidXMLFileFormatException, IOException {
       /*
        app = new CourseSiteGenerator();
        app.loadProperties("app_properties.xml");
        app.buildAppComponentsHookTest();
        csgData dataComp = (csgData) app.getDataComponent();
        
        
        
        //csgData dataManager = (csgData) app.getDataComponent();
        TeachingAssistant ta1 =  new TeachingAssistant("Jane Doe","jane.doe@yahoo.com",true);
        TeachingAssistant ta2 =  new TeachingAssistant("Joe Shmo","joe.shmo@yale.com",false);
        recitationItem rec1 = new recitationItem("R02", "McKenna", "Wed 3:30pm-4:23pm", "Old CS 2114", "Jane Doe", "Joe Shmo");       
        recitationItem rec2 =  new recitationItem("R05", "Banerjee", "Tues 5:30pm-6:23pm", "Old CS 2114", "", "" );
        
        timeSlotData.add(new TimeSlot("MONDAY", "9_00am", "Usman"));
        timeSlotData.add(new TimeSlot("TUESDAY", "9_00am", "Namsu"));
        
         dataComp.addTA(ta1.getName(),ta1.getEmail(),ta1.getUnd());
        dataComp.addTA(ta2.getName(),ta2.getEmail(),ta2.getUnd());
        dataComp.addRec(rec1.getSection(),rec1.getInstructor(),rec1.getDayTime(),rec1.getLocation(),rec1.getTA1(),rec1.getTA2());
        dataComp.addRec(rec2.getSection(),rec2.getInstructor(),rec2.getDayTime(),rec2.getLocation(),rec2.getTA1(),rec2.getTA2());
         
        
        csgFiles fileManager = new csgFiles();
        String filePath = "C:\\Users\\Usman\\Documents\\Homework 4\\CourseSiteGenerator\\work\\SiteSaveTest.json";
        fileManager.saveData(dataComp, filePath);
        
        /*
        officeHours.put("0_1", new SimpleStringProperty("9:00am"));
        officeHours.put("0_2", new SimpleStringProperty("9:30am"));
        officeHours.put("2_0", new SimpleStringProperty("MONDAY"));
        officeHours.put("3_0", new SimpleStringProperty("TUESDAY"));
        officeHours.put("4_0", new SimpleStringProperty("WEDNESDAY"));
        officeHours.put("5_0", new SimpleStringProperty("THURSDAY"));
        officeHours.put("6_0", new SimpleStringProperty("FRIDAY"));
       
       
        officeHours.put("2_1", new SimpleStringProperty("Jane Doe"));
        
        officeHours.put("3_1", new SimpleStringProperty("Jane Doe"));
        officeHours.put("4_1", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("5_1", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("6_1", new SimpleStringProperty("Joe Shmo"));
        
        officeHours.put("2_2", new SimpleStringProperty("Jane Doe"));
        officeHours.put("3_2", new SimpleStringProperty("Jane Doe"));
        officeHours.put("4_2", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("5_2", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("6_2", new SimpleStringProperty("Joe Shmo")); 
        */
        /*
        dataComp.addOfficeHoursReservation("MONDAY", "9_00am", "Usman");
        dataComp.addOfficeHoursReservation("TUESDAY", "9_00am", "Namsu");
        dataComp.addOfficeHoursReservation("WEDNESDAY", "9_00am", "Yo");
        dataComp.addOfficeHoursReservation("THURSDAY", "9_00am", "yyo");
        dataComp.addOfficeHoursReservation("FRIDAY", "9_00am", "yyoo");
        
        dataComp.addOfficeHoursReservation("MONDAY", "9_30am", "Usman");
        dataComp.addOfficeHoursReservation("TUESDAY", "9_30am", "Namsu");
        dataComp.addOfficeHoursReservation("WEDNESDAY", "9_30am", "Yo");
        dataComp.addOfficeHoursReservation("THURSDAY", "9_30am", "yyo");
        dataComp.addOfficeHoursReservation("FRIDAY", "9_30am", "yyoo");
        officeHours
        //dataComp.setCellProperty(2, 0, prop); */
        
        
        
} 
    public static void main(String [] args) throws InvalidXMLFileFormatException, IOException{
//       Locale.setDefault(Locale.US);
//	try{
//        launch(args);}
//        catch(Exception e){
//            System.out.println(e);
//        }
    saveData();
    }

  
    public static void saveData() throws IOException {
        CourseSiteGenerator app = new CourseSiteGenerator();
        //app.start(primaryStage);
        app.loadProperties("app_properties.xml");
        app.buildAppComponentsHookTest();
       //app.getGUI().getWindow().close();
       
        csgData dataComp = (csgData) app.getDataComponent();
        dataComp.setTimes(9, 10);
        //dataComp =  (csgData) app.getDataComponent();
        HashMap<String,StringProperty> officeHours = new HashMap();
        
        //csgData dataManager = (csgData) app.getDataComponent();
        TeachingAssistant ta1 =  new TeachingAssistant("Jane Doe","jane.doe@yahoo.com",true);
        TeachingAssistant ta2 =  new TeachingAssistant("Joe Shmo","joe.shmo@yale.com",false);
        recitationItem rec1 = new recitationItem("R02", "McKenna", "Wed 3:30pm-4:23pm", "Old CS 2114", "Yo", "Joe");       
        recitationItem rec2 =  new recitationItem("R05", "Banerjee", "Tues 5:30pm-6:23pm", "Old CS 2114", "", "" );
        scheduleItem sch1 = new scheduleItem("Holiday","2/9/2017","SNOW DAY","CSE","5:00","www.hello.com","criteria1");
        scheduleItem sch2 = new scheduleItem("Lecture","3/15/2017","Lecture 3","Math","8:00","www.goodbye.com","criteria2");
        teamsItem team1 = new teamsItem("Atomic Comics", "552211", "ffffff","http://atomiccomic.com");
        SitePages site1 = new SitePages(true,"Home","index.html","HomeBuilder.js");
        SitePages site2 = new SitePages(true,"Home","index.html","HomeBuilder.js");
        studentItem student1 = new studentItem("Beau", "Brumell","Atomic Comics","Lead Designer");
        courseInfo course1 = new courseInfo("CSE","Fall","Computer Science III","Richard McKenna","www.no.com","219","2017");
        
       // timeSlotData.add(new TimeSlot("MONDAY", "9_00am", "Usman"));
       //timeSlotData.add(new TimeSlot("TUESDAY", "9_00am", "Namsu"));
        /*
       dataComp.addOfficeHoursReservation("MONDAY", "9_00am", "Usman");
       //cellKey = data
        dataComp.addOfficeHoursReservation("TUESDAY", "9_00am", "Namsu");
        dataComp.addOfficeHoursReservation("WEDNESDAY", "9_00am", "Yo");
        dataComp.addOfficeHoursReservation("THURSDAY", "9_00am", "yyo");
        dataComp.addOfficeHoursReservation("FRIDAY", "9_00am", "yyoo");
        
        dataComp.addOfficeHoursReservation("MONDAY", "9_30am", "Usman");
        dataComp.addOfficeHoursReservation("TUESDAY", "9_30am", "Namsu");
        dataComp.addOfficeHoursReservation("WEDNESDAY", "9_30am", "Yo");
        dataComp.addOfficeHoursReservation("THURSDAY", "9_30am", "yyo");
        dataComp.addOfficeHoursReservation("FRIDAY", "9_30am", "yyoo"); */
       
        officeHours.put("0_1", new SimpleStringProperty("9:00am"));
        officeHours.put("0_2", new SimpleStringProperty("9:30am"));
        officeHours.put("0_3", new SimpleStringProperty("10:00am"));
        
        officeHours.put("2_0", new SimpleStringProperty("MONDAY"));
        officeHours.put("3_0", new SimpleStringProperty("TUESDAY"));
        officeHours.put("4_0", new SimpleStringProperty("WEDNESDAY"));
        officeHours.put("5_0", new SimpleStringProperty("THURSDAY"));
        officeHours.put("6_0", new SimpleStringProperty("FRIDAY"));
       
       
        officeHours.put("2_1", new SimpleStringProperty("Jane Doe"));
        
        officeHours.put("3_1", new SimpleStringProperty("Jane Doe"));
        officeHours.put("4_1", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("5_1", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("6_1", new SimpleStringProperty("Joe Shmo"));
        
        officeHours.put("2_2", new SimpleStringProperty("Jane Doe"));
        officeHours.put("3_2", new SimpleStringProperty("Jane Doe"));
        officeHours.put("4_2", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("5_2", new SimpleStringProperty("Joe Shmo"));
        officeHours.put("6_2", new SimpleStringProperty("Joe Shmo")); 
        
        
        dataComp.setOffice(officeHours);
        dataComp.addTA(ta1.getName(),ta1.getEmail(),ta1.getUnd());
        dataComp.addTA(ta2.getName(),ta2.getEmail(),ta2.getUnd());
        dataComp.addRec(rec1.getSection(),rec1.getInstructor(),rec1.getDayTime(),rec1.getLocation(),rec1.getTA1(),rec1.getTA2());
        dataComp.addRec(rec2.getSection(),rec2.getInstructor(),rec2.getDayTime(),rec2.getLocation(),rec2.getTA1(),rec2.getTA2());
        dataComp.addSch(sch1.getType(),sch1.getDate(),sch1.getTitle(),sch1.getTopic(),sch1.getTime(),sch1.getLink(),sch1.getCriteria());
        dataComp.addSch(sch2.getType(),sch2.getDate(),sch2.getTitle(),sch2.getTopic(),sch2.getTime(),sch2.getLink(),sch2.getCriteria());
        dataComp.addTeam(team1.getName(),team1.getColor(),team1.getTextColor(),team1.getLink());
        dataComp.addStudent(student1.getFName(),student1.getLName(),student1.getTeamName(),student1.getRole());
        dataComp.addSite(site1.getUse(), site1.getTitle(), site1.getFileName(), site1.getScript());
        dataComp.addSite(site2.getUse(), site2.getTitle(), site2.getFileName(), site2.getScript());
        dataComp.addCourseInfo(course1.getSubject(),course1.getSemester(),course1.getTitle(),course1.getInstrucName(), course1.getInstrucHome(), course1.getNumber(),course1.getYear());
        
        csgFiles fileManager = new csgFiles();
        String filePath = "C:\\Users\\Usman\\Documents\\Homework 4\\CourseSiteGenerator\\work\\SiteSaveTest.json";
        
        fileManager.saveData(dataComp, filePath);
       
    }
        
}
/*
    @Override
    public void start(Stage primaryStage)  {
        //TestSave obj = new TestSave();
        app = new CourseSiteGenerator();
        app.start(primaryStage);
        if(app!=null && app.getDataComponent()!=null){
            System.out.println("Got here");
        }
        
        saveCode();
        
        
        /*
        try {
            fileManager.saveData(dataManager,filePath);
        } catch (IOException ex) {
            Logger.getLogger(TestSave.class.getName()).log(Level.SEVERE, null, ex);
        }
    } */
    



