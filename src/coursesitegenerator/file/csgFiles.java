
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.file;

import coursesitegenerator.CourseSiteGenerator;
import coursesitegenerator.data.SitePages;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.courseInfo;
import coursesitegenerator.data.csgData;
import coursesitegenerator.data.recitationItem;
import coursesitegenerator.data.scheduleItem;
import coursesitegenerator.data.studentItem;
import coursesitegenerator.data.teamsItem;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Usman
 */
public class csgFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    CourseSiteGenerator app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UND = "undergrad";
    static final String JSON_USE = "use";
    static final String JSON_FILENAME = "fileName";
    static final String JSON_TITLEC = "title";
    static final String JSON_SCRIPT = "script";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_RECITATIONS = "recitations";
    static final String JSON_SCHEDULES = "schedules";
    static final String JSON_STUDENTS = "students";
    static final String JSON_COURSEINFO = "courses";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_TITLECI = "titleci";
    static final String JSON_INSTRUCNAME = "instrucName";
    static final String JSON_INSTRUCHOME = "instrucHome";
    static final String JSON_NUMBER = "number";
    static final String JSON_YEAR = "year";
    static final String JSON_SITEPAGES = "SitePages";
    static final String JSON_TEAMS = "teams";
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAYTIME = "dayTime";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta1";
    static final String JSON_TA2 = "ta2";
    static final String JSON_TYPE = "type";
    static final String JSON_DATE = "date";
    static final String JSON_TITLE = "title";
    static final String JSON_TOPIC = "topic";
    static final String JSON_TIMES = "time";
    static final String JSON_LINK = "link";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_NAMEP = "name";
    static final String JSON_COLOR = "color";
    static final String JSON_TEXTCOLOR = "textColor";
    static final String JSON_LINKP = "link";
    static final String JSON_FNAME = "fName";
    static final String JSON_LNAME = "lName";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_EMAIL = "email";

    public csgFiles(CourseSiteGenerator initApp) {
        app = initApp;
    }
    public csgFiles(){
        
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
	csgData dataManager = (csgData)data;
        
        JsonArrayBuilder courseArrayBuilder = Json.createArrayBuilder();
	ObservableList<courseInfo> courses = dataManager.getCourseInfo();
	for (courseInfo course : courses) {	    
	    JsonObject courseJson = Json.createObjectBuilder()
		    .add(JSON_SUBJECT, course.getSubject())
                    .add(JSON_SEMESTER, course.getSemester())
                    .add(JSON_TITLE, course.getTitle())
                    .add(JSON_INSTRUCNAME, course.getInstrucName())
                    .add(JSON_INSTRUCHOME, course.getInstrucHome())
                    .add(JSON_NUMBER, course.getNumber())
                    .add(JSON_YEAR, course.getYear())
                    .build();
	    courseArrayBuilder.add(courseJson);
	}
	JsonArray courseArray = courseArrayBuilder.build();
        
        
        JsonArrayBuilder pageArrayBuilder = Json.createArrayBuilder();
	ObservableList<SitePages> sites = dataManager.getSitePagesList();
	for (SitePages site : sites) {	    
	    JsonObject siteJson = Json.createObjectBuilder()
		    .add(JSON_USE, site.getUse())
                    .add(JSON_TITLE, site.getTitle())
                    .add(JSON_FILENAME, site.getFileName())
                    .add(JSON_SCRIPT, site.getScript())
                    .build();
	    pageArrayBuilder.add(siteJson);
	}
	JsonArray pageArray = pageArrayBuilder.build();
        
        
        
	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL,ta.getEmail())
                    .add(JSON_UND, ta.getUnd())
                    .build();
	    taArrayBuilder.add(taJson);
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();
        
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
        ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
        
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
        JsonArrayBuilder recArrayBuilder = Json.createArrayBuilder();
	ObservableList<recitationItem> recs = dataManager.getRecitations();
	for (recitationItem rec : recs) {	    
	    JsonObject recJson = Json.createObjectBuilder()
		    .add(JSON_SECTION, rec.getSection())
                    .add(JSON_INSTRUCTOR,rec.getInstructor())
                    .add(JSON_DAYTIME, rec.getDayTime())
                    .add(JSON_LOCATION,rec.getLocation())
                    .add(JSON_TA1, rec.getTA1())
                    .add(JSON_TA2,rec.getTA2())
                    .build();
	    recArrayBuilder.add(recJson);
	}
	JsonArray recitationArray = recArrayBuilder.build();
        
        JsonArrayBuilder schArrayBuilder = Json.createArrayBuilder();
	ObservableList<scheduleItem> schs = dataManager.getSchedules();
	for (scheduleItem sch : schs) {	    
	    JsonObject schJson = Json.createObjectBuilder()
		    .add(JSON_TYPE, sch.getType())
                    .add(JSON_DATE, sch.getDate())
                    .add(JSON_TITLE, sch.getTitle())
                    .add(JSON_TOPIC, sch.getTopic())
                    .add(JSON_TIMES, sch.getTime())
                    .add(JSON_LINK, sch.getLink())
                    .add(JSON_CRITERIA, sch.getCriteria())
                    .build();
	    schArrayBuilder.add(schJson);
	}
	JsonArray scheduleArray = schArrayBuilder.build();
        
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
	ObservableList<teamsItem> teams = dataManager.getTeams();
	for (teamsItem team : teams) {	    
	    JsonObject teamJson = Json.createObjectBuilder()
		    .add(JSON_NAMEP, team.getName())
                    .add(JSON_COLOR, team.getColor())
                    .add(JSON_TEXTCOLOR, team.getTextColor())
                    .add(JSON_LINKP, team.getLink())
                    .build();
	    teamArrayBuilder.add(teamJson);
	}
	JsonArray teamArray = teamArrayBuilder.build();
        
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
	ObservableList<studentItem> students = dataManager.getStudents();
	for (studentItem student : students) {	    
	    JsonObject studentJson = Json.createObjectBuilder()
		    .add(JSON_FNAME, student.getFName())
                    .add(JSON_LNAME, student.getLName())
                    .add(JSON_TEAM, student.getTeamName())
                    .add(JSON_ROLE, student.getRole())
                    .build();
	    studentArrayBuilder.add(studentJson);
	}
	JsonArray studentArray = studentArrayBuilder.build();
	
        // THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_COURSEINFO, courseArray)
                .add(JSON_SITEPAGES, pageArray)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                .add(JSON_RECITATIONS, recitationArray)
                .add(JSON_SCHEDULES, scheduleArray)
                .add(JSON_TEAMS, teamArray)
                .add(JSON_STUDENTS, studentArray)
                
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    public JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        csgData dataManager = (csgData)data;
        System.out.println(filePath);
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            Boolean undergrad = jsonTA.getBoolean(JSON_UND);
            
           // dataManager.addTA(name);
            dataManager.addTA(name,email,undergrad);
        }
        System.out.println("Load ta");
        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        JsonArray jsonSitePagesArray = json.getJsonArray(JSON_SITEPAGES);
        for (int i = 0; i < jsonSitePagesArray.size(); i++) {
            JsonObject jsonSitePages = jsonSitePagesArray.getJsonObject(i);
            Boolean use = jsonSitePages.getBoolean(JSON_USE);
            String titleC = jsonSitePages.getString(JSON_TITLEC);
            String fileName = jsonSitePages.getString(JSON_FILENAME);
            String script = jsonSitePages.getString(JSON_SCRIPT);
            
            dataManager.addSite(use, titleC, fileName, script);
        }
        
        System.out.println("Load OH");
        JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATIONS);
        for (int i = 0; i < jsonRecitationArray.size(); i++) {
            JsonObject jsonRecitations = jsonRecitationArray.getJsonObject(i);
            String section = jsonRecitations.getString(JSON_SECTION);
            String instructor = jsonRecitations.getString(JSON_INSTRUCTOR);
            String dayTime = jsonRecitations.getString(JSON_DAYTIME);
            String location = jsonRecitations.getString(JSON_LOCATION);
            String ta1 = jsonRecitations.getString(JSON_TA1);
            String ta2 = jsonRecitations.getString(JSON_TA2);
            dataManager.addRec(section, instructor, dayTime, location, ta1, ta2);
        }
        System.out.println("Load rec");
        JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULES);
        for (int i = 0; i < jsonScheduleArray.size(); i++) {
            JsonObject jsonSchedules = jsonScheduleArray.getJsonObject(i);
            String type = jsonSchedules.getString(JSON_TYPE);
            String date = jsonSchedules.getString(JSON_DATE);
            String title = jsonSchedules.getString(JSON_TITLE);
            String topic = jsonSchedules.getString(JSON_TOPIC);
            String time = jsonSchedules.getString(JSON_TIMES);
            String link = jsonSchedules.getString(JSON_LINK);
            String criteria = jsonSchedules.getString(JSON_CRITERIA);
            dataManager.addSch(type, date, title, topic, time, link, criteria);
        }
        System.out.println("Load sched");
        JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
        for (int i = 0; i < jsonTeamArray.size(); i++) {
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String namep = jsonTeam.getString(JSON_NAMEP);
            String color = jsonTeam.getString(JSON_COLOR);
            String textColor = jsonTeam.getString(JSON_TEXTCOLOR);
            String linkp = jsonTeam.getString(JSON_LINK);
           dataManager.addTeam(namep, color, textColor, linkp);
        }
        System.out.println("Te");
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
        for (int i = 0; i < jsonStudentArray.size(); i++) {
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String fName = jsonStudent.getString(JSON_FNAME);
            String lName = jsonStudent.getString(JSON_LNAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
           dataManager.addStudent(fName, lName, team, role);
        }
        
        System.out.println("Stu 0 done");
        
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
